<?php

// Prevent file from being accessed directly
if (!defined('ABSPATH')) exit();

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'xynomixw_site');

/** MySQL database username */
define('DB_USER', 'xynomixw_user');

/** MySQL database password */
define('DB_PASSWORD', '$Fr4m3w0rk$');

/** MySQL hostname */
define('DB_HOST', '10.169.0.172');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '*-yGl@pT!EdrdeT`tRjw[#{U|lLa%WL&0X3Sf{OY5*eU)+|Ssv<sX`2;]+bQM6EC');
 define('SECURE_AUTH_KEY',  'Rw4E|IQY_1OYBk[.]+ <z)[boQ*0(RnDTnz~3X`iQKmxn>}w*X!fx^@eO#:9n]*)');
 define('LOGGED_IN_KEY',    ':E*4r-l?GI>O=]^jv,gS-etk]{e $DN^-l7YG4{n^F@]|(pGdt,f<Y3/w><pPZ%F');
 define('NONCE_KEY',        '<CDxzidx!uP1(k]ryqmyxoF.{QjV3DHf>{jD:D5G$_M2Xg0aW= .mr|/Ca-+Q)&|');
 define('AUTH_SALT',        '(1(ukf0R,}3zQC>/Q>oGq][Zw?{gme|{bv5sXz00DJ[^E+/}xf+!@| A?ih8ABv#');
 define('SECURE_AUTH_SALT', '1h/9B(>(oL1,1y6Ha9&{P=.:#36/)D+{6us%D<7xuo hj | BUBMw^{/G=r~JpIR');
 define('LOGGED_IN_SALT',   '0D+w#^lOTU4)5OAr2$b2*Vs5PARGvjr<4BS``p7NpFfr$Sm6.i|*(N+G=NT^|%,0');
 define('NONCE_SALT',       'V H&~F />*4GQki5,;RvriZr,R=l}m-J0Y(oko1^iSpE^!z-^hT(c%W7+:W_IXWy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

$login_successful = false;

if ( isset( $_SERVER['PHP_AUTH_USER'] ) && isset( $_SERVER['PHP_AUTH_PW'] ) ) {

	$username = $_SERVER['PHP_AUTH_USER'];
	$password = $_SERVER['PHP_AUTH_PW'];

	if ($username === 'demo' && $password === 'sitename123'){
		$login_successful = true;
	}

}

if ( !$login_successful ){
	header('WWW-Authenticate: Basic realm="Framework Staging"');
	header('HTTP/1.0 401 Unauthorized');
	die( '<center><h1>401 Authorization Required</h1></center>' );
}
