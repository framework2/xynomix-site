<?php

// Prevent file from being accessed directly
if (!defined('ABSPATH')) exit();

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'xynomix');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '14?vU{llXRu/+p6rLW#GTjfCPmU=J[vivL1L7B#6V?$q&I`:)6sv[wDNBSq/km~.');
define('SECURE_AUTH_KEY',  'l3^-ah;7 %)-Gw.lN) -76qk+K-BzL5=W71AvALL_n*4T<:z*g%UE:&,|[pXo>z=');
define('LOGGED_IN_KEY',    '2)3N(q])EF3>1UH+y~zS5a)D-n>+9#bd,;Q9>oX !c! :G8wr2`YIYQIHWZ7RJXr');
define('NONCE_KEY',        '-,.=|B4&+,B&-IKzC9C0rw),+TRd^Jg|GecB@~;A4SC(%}<.Xm*,;CO,2O*`/Eh~');
define('AUTH_SALT',        'pV34*m^i7Rp-r72(4b+i*jCppdjYka6|qL!2jO9$O*D)yS|E/o+Gf]f)%~kTi-IJ');
define('SECURE_AUTH_SALT', 'Q*2O(_Tjk&Vqa+ p~/DGC,|+m4Sbtqpde?J3%?s&]4sA9i#+:y`.`+Fr^-f2BBu%');
define('LOGGED_IN_SALT',   'v-=].0-631J=*=VR4LOsxGI+a]2]C[a][1F,Uj~jmUUfe$Q#4,Uq[vQ#tUYuPY%E');
define('NONCE_SALT',       'Mox~Wy i<iP^j_Ap/ZLjPz_*dilh{&3@.WQl&JdZ&]=--mwjvi9aD-!H-k+Oyt38');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);