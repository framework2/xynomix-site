<?php

// Prevent file from being accessed directly
if (!defined('ABSPATH')) exit();

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'xynomix_live');

/** MySQL database username */
define('DB_USER', 'xynomix_user');

/** MySQL database password */
define('DB_PASSWORD', '6*ZZUu,&gR*f');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%6)/r2p/}~+S|MoP|o~VHq6UCYq!|oe{e<]p[(*-+-</fD6FD4w=Kpuf+*7T5B7r');
define('SECURE_AUTH_KEY',  '_^4$R-Z_pQ-JqE61=--N7ZJ-{ha|ux0DHx5#Fvx[:dgSmG$k7L#:p8r=gZ+a|>#L');
define('LOGGED_IN_KEY',    'jQ@[?68w$||N6?tY_:kRl>Y`)=K*bp.4]/@iHQ@$d1`b7Gr+fm^W38i_55KopNf_');
define('NONCE_KEY',        'Fkm%5$~-%uhG|w}zx@3R<)T95*F&~UepGcrJA5A>M|m4&mj!@pe1I.1MBK+bfHkM');
define('AUTH_SALT',        '%kp ;EHQ-X&d38AnwXW<HVAl -/2N(wY`7:$<Bsmeyu1+TanBg:fZHP*|4mMwtTE');
define('SECURE_AUTH_SALT', 'yOl:q.m</W3xtOF0lm~?a9_^A.M],B$6Y7/>&<G_bmBFcGGt2&WcC**vL/((GKhq');
define('LOGGED_IN_SALT',   'KAw4Y67%C+*0(c_:&*+a4Y(XLny-+),_Fu+bl*T#t>/[Ho=^N& %OC L;U&iACCq');
define('NONCE_SALT',       'b&D{WhC@ZR_|BFRTkveqrx@Lf. }6Rx2}+Yqen[<r7U4z;7;ig1ZR~mPUuVPU|G*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);