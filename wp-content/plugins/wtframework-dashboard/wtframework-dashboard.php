<?php
/*
Plugin Name: The Framework Dashboard
Plugin URI:  https://weareframework.co.uk
Description: Plugin to add The Framework branding to the site
Version:     1.0.0
Author:      weareframework
Author URI:  https://developer.wordpress.org/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*
 * Define Constants
 */
define('WTF_DASHBOARD_FILE', __FILE__);
define('WTF_DASHBOARD_DIR', dirname(__FILE__));
define('WTF_DASHBOARD_BASE', plugin_basename(__FILE__));

require_once( plugin_dir_path( __FILE__ ) .'vendor/autoload.php' );

function run_plugin() {

	$plugin = new \TWFramework\Init();
	$plugin->run();

}

run_plugin();