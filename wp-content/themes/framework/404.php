<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<div class="small:mt-6 large:-mt-4">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>
<?php get_footer(); ?>
