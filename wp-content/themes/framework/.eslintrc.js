module.exports = {
    "extends": "airbnb-base",
    "env": {
        "node": true, // this is the best starting point
        "es6": true // enables es6 features
    },
    "plugins": [
        "html"
    ],
    "parser": "babel-eslint",
    "rules": {
        "strict": 0,
        "no-console": "off",
        "no-unused-vars": 0,
        "indent": 0,
        "radix": "off",
        "max-len": "off",
        "no-param-reassign": "off",
        "eol-last": "off",
        "class-methods-use-this": "off",
        "no-trailing-spaces": "off",
        "padded-blocks": "off",
        "spaced-comment": "off",
        "no-tabs": "off",
        "no-new": "off"
    },
    "globals": {
        "_": false,
        "window": true,
        "document": true,
        "Breakpoint": true,
        "Flickity": true,
        "app": true,
        '$': true,
        'google': true,
        'jQuery': true,
        'animateScroll': true,
        'scrollMonitor': true,
        'TweenLite': true,
        'TweenMax': true,
        "wpcf7": true,
        'Image': true,
        'Headroom': true,
        'axios': true,
        'config': true,
        'Masonry': true,
        'imagesLoaded': true,
    }
};