<?php

namespace TheFramework\Svg;

if ( !defined('ABSPATH') ) exit;

trait Svg {

	public static function svgIcon($icon = null, $viewBox = '0 0 100 100') {
		if(is_null($icon)){
			return '';
		}
		echo '<svg viewBox="'.$viewBox.'" class="c-icon">
	    <use xlink:href="#' . $icon . '"></use>
	  </svg>';
	}
}