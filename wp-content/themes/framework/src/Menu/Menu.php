<?php

namespace TheFramework\Menu;

if ( !defined('ABSPATH') ) exit;

class Menu {

	private $flat_menu;
	public $items;

	public function __construct( $name ) {

		$this->flat_menu = wp_get_nav_menu_items($name);
		$this->items = [];
		foreach ($this->flat_menu as $item) {
			if (!$item->menu_item_parent) {
				array_push($this->items, $item);
			}
		}
	}

	public function get_submenu( $item ) {

		$this->submenu = [];
		foreach ($this->flat_menu as $subitem) {
			if ($subitem->menu_item_parent == $item->ID) {
				array_push($this->submenu, $subitem);
			}
		}

		return $this->submenu;
	}
}