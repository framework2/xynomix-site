<?php

namespace TheFramework\CT;

if ( !defined('ABSPATH') ) exit;

class CustomTaxonomy {

	protected $text_domain;
	protected $taxonomies = [];

	public function __construct () {
		$this->text_domain = str_replace( ' ', '-', strtolower(get_bloginfo('name')));
		add_action('init', [$this, 'register_taxonomy'], 0 );
	}

	public function register_taxonomy() {
		foreach( $this->taxonomies as $key => $value ) {
			register_taxonomy( $key, $value['post_types'], $value['args'] );
		}
	}

	public function add( $type, $singular, $plural, $post_types, $settings = [] ) {
		$default = [
			'labels' => [
				'name' => __($plural, $this->text_domain),
				'singular_name' => __($singular, $this->text_domain),
				'add_new_item' => __('New '.$singular.' Name', $this->text_domain),
				'separate_items_with_commas' => __('Separate '.strtolower($plural).' with commas', $this->text_domain),
				'new_item_name' => __('Add New '.$singular, $this->text_domain),
				'edit_item' => __('Edit '.$singular, $this->text_domain),
				'update_item' => __('Update '.$singular, $this->text_domain),
				'add_or_remove_items' => __('Add or remove '.strtolower($plural), $this->text_domain),
				'search_items' =>__('Search '.$plural, $this->text_domain),
				'popular_items' =>__('Popular '.$plural, $this->text_domain),
				'all_items' =>__('All '.$plural, $this->text_domain),
				'parent_item' =>__('Parent '.$singular, $this->text_domain),
				'choose_from_most_used' => __('Choose from the most used '.strtolower( $plural ), $this->text_domain),
				'parent_item_colon' =>__('Parent '.$singular, $this->text_domain),
			],
			'public'   => true,
			'rewrite'  => [
				'slug' => sanitize_title_with_dashes( $plural )
			]
		];

		$this->taxonomies[$type]['post_types'] = $post_types;
		$this->taxonomies[$type]['args'] = array_merge( $default, $settings );
	}
}