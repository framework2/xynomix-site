<?php 
namespace TheFramework\Components;

if ( !defined('ABSPATH') ) exit;

use TheFramework\View; 

trait Buttons {

	public static function btnPrimary ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnPrimary.php';
        $view_data = [
            'text' => $text,
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnPrimaryAlt ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnPrimaryAlt.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnPrimaryAlt2 ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnPrimaryAlt2.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnPrimaryAlt3 ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnPrimaryAlt3.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }    

    public static function btnSecondary ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnSecondary.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnSecondaryAlt ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnSecondaryAlt.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnSecondaryAlt2 ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnSecondaryAlt2.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnArrow ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnArrow.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnCircle ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnCircle.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnCircleAlt ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnCircleAlt.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnCircleAlt2 ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnCircleAlt2.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnBio ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnBio.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

    public static function btnSearch ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnSearch.php';
        $view_data = [
            'text' => ucFirst($text),
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

	public static function btnAccordion ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnAccordion.php';
        $view_data = [
            'text' => $text,
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

	public static function btnBack ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnBack.php';
        $view_data = [
            'text' => $text,
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }

	public static function btnPagination ($text = null, $url = null, $classes = null, $tag = null) {
        
        $template = get_template_directory().'/src/Components/Partials/btnPagination.php';
        $view_data = [
            'text' => $text,
            'link' => $url,
            'classes' => $classes,
            'tag' => $tag
        ];

        echo View::render($template, $view_data);
    }
}

?>