<?php if ( is_null($tag) ) : ?>

    <a href="<?php if (!is_null($link)) echo $link; ?>" class="btn btn--primary-alt <?php if (!is_null($classes)) echo $classes; ?>">
        <?php echo $text; ?>
    </a>

<?php else : ?>

    <<?php echo $tag; ?> class="btn btn--primary-alt <?php if (!is_null($classes)) echo $classes; ?>">
        <?php echo $text; ?>
    </<?php echo $tag; ?>>

<?php endif; ?>