<?php if ( is_null($tag) ) : ?>

    <a href="<?php if (!is_null($link)) echo $link; ?>" class="btn btn--circle-alt2 <?php if (!is_null($classes)) echo $classes; ?>">
        <div class="btn--circle__icon">
            <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
        </div>
    </a>

<?php else : ?>

    <<?php echo $tag; ?> class="btn btn--circle-alt2 <?php if (!is_null($classes)) echo $classes; ?>">
        <div class="btn--circle__icon">
            <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
        </div>
    </<?php echo $tag; ?>>    

<?php endif; ?>