<?php if ( is_null($tag) ) : ?>

    <a href="<?php if (!is_null($link)) echo $link; ?>" class="btn btn--accordion <?php if (!is_null($classes)) echo $classes; ?>">
    </a>

<?php else : ?>

    <<?php echo $tag; ?> class="btn btn--accordion <?php if (!is_null($classes)) echo $classes; ?>">
    </<?php echo $tag; ?>>

<?php endif; ?>