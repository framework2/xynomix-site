<?php if ( is_null($tag) ) : ?>

    <a href="<?php if (!is_null($link)) echo $link; ?>" class="btn btn--search <?php if (!is_null($classes)) echo $classes; ?>">
        <div class="btn--search__icon">
            <?php Framework::svgIcon('search', '0 0 16 16'); ?>
        </div>
    </a>

<?php else : ?>

    <<?php echo $tag; ?> class="btn btn--search <?php if (!is_null($classes)) echo $classes; ?>">
        <div class="btn--search__icon">
            <?php Framework::svgIcon('search', '0 0 16 16'); ?>
        </div>
    </<?php echo $tag; ?>>    

<?php endif; ?>