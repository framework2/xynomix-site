<?php if ( is_null($tag) ) : ?>

    <a href="<?php if (!is_null($link)) echo $link; ?>" class="btn btn--arrow <?php if (!is_null($classes)) echo $classes; ?>">
	    <div class="btn--arrow__icon">
		    <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
	    </div>
	    <span class="btn--arrow__text block"><?php echo $text; ?></span>
    </a>

<?php else : ?>

    <<?php echo $tag; ?> class="btn btn--arrow <?php if (!is_null($classes)) echo $classes; ?>">
	    <div class="btn--arrow__icon">
		    <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
	    </div>
	    <span class="btn--arrow__text block"><?php echo $text; ?></span>
    </<?php echo $tag; ?>>

<?php endif; ?>