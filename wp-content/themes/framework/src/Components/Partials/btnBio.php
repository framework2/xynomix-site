<?php if ( is_null($tag) ) : ?>

    <a href="<?php if (!is_null($link)) echo $link; ?>" class="btn btn--bio <?php if (!is_null($classes)) echo $classes; ?>">
        <?php echo $text; ?>
    </a>

<?php else : ?>

    <<?php echo $tag; ?> class="btn btn--bio <?php if (!is_null($classes)) echo $classes; ?>">
        <span class="btn--bio__text"><?php echo $text; ?></span>
        <div class="btn--bio__icon"></div>
    </<?php echo $tag; ?>>

<?php endif; ?>