<?php

/**
 * Trim words
 *
 * @param $content
 * @param int $word_count
 * @return array|string
 */
if (!function_exists('trim_words')) {
    function trim_words($content, $word_count = 10)
    {
        $string = explode(' ', $content);
        if (empty($string) == false) {
            $string = array_chunk($string, $word_count);
            $string = $string[0];
        }

        return implode(' ', $string);
    }
}


/**
 * check if an array is empty
 *
 * @return boolean
 */
if (!function_exists('is_array_empty')) {
    function is_array_empty($data)
    {
        return (!empty($data) && count($data) > 0) ? false : true;
    }
}

/**
 * Format post_content
 *
 * @param $content
 *
 * @return mixed|void
 */
if (!function_exists('format_post_content')) {
    function format_post_content($content)
    {
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);

        return $content;
    }
}

if (!function_exists('get_posts_years_array')) {
    function get_posts_years_array($args)
    {

        global $wpdb;
        $result = [];

        $defaults = [
            'post_type' => 'post',
            'post_status' => 'publish'
        ];

        $values = array_merge($defaults, $args);

        $field = 'post_status';
        $field_type = 'post_type';
        $meta_key = $wpdb->postmeta . '.meta_key';
        $meta_value = 'event_details_start_date';

        $condition = '=';

        // $sql = "SELECT EXTRACT(YEAR_MONTH FROM wp_postmeta.meta_value) FROM wp_postmeta INNER JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id AND wp_postmeta.meta_key = 'event_details_date' AND post_type = 'events' AND post_status = 'publish' GROUP BY EXTRACT(YEAR_MONTH FROM wp_postmeta.meta_value) ASC";

        $sql = "SELECT EXTRACT(YEAR_MONTH FROM {$wpdb->postmeta}.meta_value) FROM {$wpdb->postmeta} INNER JOIN {$wpdb->posts} ON {$wpdb->posts}.ID = {$wpdb->postmeta}.post_id AND ";
        $sql .= $wpdb->prepare($meta_key . ' ' . $condition . ' %s', $meta_value);
        $sql .= " AND ";
        $sql .= $wpdb->prepare($field_type . ' ' . $condition . ' %s', $values['post_type']);
        $sql .= " AND ";
        $sql .= $wpdb->prepare($field . ' ' . $condition . ' %s', $values['post_status']);
        $sql .= "GROUP BY EXTRACT(YEAR_MONTH FROM {$wpdb->postmeta}.meta_value) ASC";

        $years = $wpdb->get_results($sql, ARRAY_N);

        if (is_array($years) && count($years) > 0) {
            foreach ($years as $year) {
                $dateTime = DateTime::createFromFormat('Ym', $year[0]);
                array_push($result, [
                    'month' => $dateTime->format('F'),
                    'year' => $dateTime->format('Y')
                ]);
            }
        }

        return $result;
    }
}