<?php

/**
 * Debugging code
 */
if ( !function_exists('debug')) {
	function debug(){
		$vars = func_get_args();
		$flag = end( $vars );
		echo '<pre>';
		array_map(function( $var ){
			print_r( $var );
			echo '<br>';
		}, $vars);
		echo '</pre>';
		if ( is_bool( $flag ) && $flag == true ) die;
	}
}