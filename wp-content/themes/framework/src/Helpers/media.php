<?php

/**
 * Get url of post thumbnail
 *
 * @param $post
 * @param null $size
 * @return mixed
 */
if( !function_exists('get_thumbnail_url')) {
	function get_thumbnail_url( $post_id, $size = 'thumbnail' ) {
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size) ;
		return $image['0'];
	}
}

/**
 * Get all featured sizes
 *
 * @param  $post_id
 * @return array|string
 */
if ( !function_exists('get_all_featured_sizes')) {

	function get_all_featured_sizes($post_id) {
		$sizes  = get_intermediate_image_sizes();
		$images = [];

		foreach ($sizes as $size ) {
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );
			$images[$size] = $image[0];
		}

		return $images;
	}
}

/**
 * Return youtube video id from youtube url
 */
if ( !function_exists('get_youtube_id')) {
	function get_youtube_id($youtube_url) {
		$pattern =
			'%^# Match any youtube URL
        (?:https?://)?  # Optional scheme. Either http or https
        (?:www\.)?      # Optional www subdomain
        (?:             # Group host alternatives
          youtu\.be/    # Either youtu.be,
        | youtube\.com  # or youtube.com
          (?:           # Group path alternatives
            /embed/     # Either /embed/
          | /v/         # or /v/
          | /watch\?v=  # or /watch\?v=
          )             # End path alternatives.
        )               # End host alternatives.
        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
        $%x';

		$result = preg_match($pattern, $youtube_url, $matches);
		if ($result) {
			return $matches[1];
		}
		return '';
	}
}