<?php

namespace TheFramework\CPT;

if ( !defined('ABSPATH') ) exit;

class CustomPostType {

	protected $posts = [];

	public function __construct() {
		$this->text_domain = str_replace( ' ', '-', strtolower(get_bloginfo('name')));
		add_action( 'init', [$this, 'register_custom_post'], 0 );
	}

	/**
	 * Loop through and register individual post type
	 */
	public function register_custom_post() {
		foreach( $this->posts as $key => $value ) {
			register_post_type( $key, $value );
		}
	}

	/**
	 * @param $post_type
	 * @param $singular
	 * @param $plural
	 * @param array $settings
	 */
	public function create( $post_type, $singular, $plural, $settings = [] ) {

		$default = [
			'labels' => [
				'name'  => __( $plural, $this->text_domain ),
				'singular_name' => __( $singular, $this->text_domain ),
				'add_new_item' => __( 'Add New '.$singular, $this->text_domain ),
				'edit_item' => __( 'Edit '.$singular, $this->text_domain ),
				'new_item' => __( 'New '.$singular, $this->text_domain ),
				'view_item' => __( 'View '.$singular, $this->text_domain ),
				'search_items'=> __( 'Search '.$plural, $this->text_domain ),
				'not_found' => __( 'No '.$plural.' found', $this->text_domain ),
				'not_found_in_trash' => __( 'No '.$plural.' found in trash', $this->text_domain ),
				'parent_item_colon' => __( 'Parent '.$singular, $this->text_domain ),
			],
			'hierarchical' => true,
			'public' => true,
			'has_archive' => true,
			'menu_position'=> 20,
			'supports' => [ 'title', 'editor', 'thumbnail' ],
			'rewrite' => [' slug' => sanitize_title_with_dashes( $plural ) ],
			'capability_type' => 'post'
		];

		$this->posts[$post_type] = array_merge( $default, $settings );
	}
}