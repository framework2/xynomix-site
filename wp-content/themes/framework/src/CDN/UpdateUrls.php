<?php namespace TheFramework\CDN;

use \TheFramework\CDN\FW_CDN_Enabler_Rewriter;
// use \TheFramework\Handle;

if ( !defined('ABSPATH') ) exit;

class UpdateUrls
{
	/**
	 * perform it
	 *
	 * @return mixed
	 */
	public static function perform($fields)
	{
		return (new static)->handle($fields);
	}


	/**
	 * return plugin options
	 *
	 * @since   0.0.1
	 * @change  1.0.3
	 *
	 * @return  array  $diff  data pairs
	 */

	public static function get_options() {
		return wp_parse_args(
			get_option('cdn_enabler'),
			array(
				'url' => get_option('home'),
				'dirs' => 'wp-content,wp-includes',
				'excludes' => '.php',
				'relative' => 1,
				'https' => 0
			)
		);
	}

	/**
	 * handle it
	 *
	 * @return mixed
	 */
	protected function handle($fields){
		if( class_exists( '\TheFramework\CDN\FW_CDN_Enabler_Rewriter' ) ) {

			// do my plugin stuff here
			$options = self::get_options();

			// check if origin equals cdn url
			if (get_option('home') == $options['url']) {
				return $fields;
			}
			// return $options;

			$excludes = array_map('trim', explode(',', $options['excludes']));
			// $homeUrl  = preg_replace('/http(?:s?):\/\//', '', get_option('home'));

			$rewriter = new FW_CDN_Enabler_Rewriter(
				get_option('home'),// $homeUrl,
				$options['url'],
				$options['dirs'],
				$excludes,
				$options['relative'],
				$options['https']
			);

			$fields = $rewriter->rewrite($fields);
		}

		return $fields;
	}

}