<?php

namespace TheFramework;

if (!defined('ABSPATH')) exit;

use \TheFramework\Svg\Svg;
use \TheFramework\Components\Buttons;

class Theme {

	use Svg;
	use Buttons;

	private $text_domain;

	public $namespace = 'xynomix/api/v1/';

	public function __construct() {
		$this->text_domain = str_replace( ' ', '-', strtolower(get_bloginfo('name')));

		add_action( 'after_setup_theme', [$this, 'theme_setup'] );
		add_action( 'wp_enqueue_scripts', [$this, 'assets' ]);
		add_action( 'widget_init', [$this, 'widgets'] );
		add_action( 'pre_get_posts', [$this, 'modify_main_query'] );
		add_action( 'acf/init', [$this, 'xynomix_acf_init']);
		add_action( 'admin_init', [$this, 'remove_extras']);
		add_action( 'wp_footer', [$this, 'api_templates']);

		add_filter( 'body_class', [$this, 'add_slug_to_body_class'] );
		add_filter( 'post_thumbnail_html', [$this, 'remove_thumbnail_dimensions'], 10 );
		add_filter( 'upload_mimes', [$this, 'allowed_mime_types'], 1, 1);
		add_filter( 'wpcf7_form_elements', [$this, 'wpcf7_tag_cleanup']);		

		if (!is_admin()) {
            add_filter( 'wpcf7_form_tag', [$this, 'contact_form_7_dynamic_field'] );
        }
	}

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @uses add_theme_support() To add support for post thumbnails and automatic feed links.
	 * @uses register_nav_menus() To add support for navigation menus.
	 * @uses set_post_thumbnail_size() To set a custom post thumbnail size.
	 */
	public function theme_setup(){

		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'featured-large', 2000, 1200, true );
		add_image_size( 'featured-medium', 100, 100, true );
		add_image_size( 'featured-small',  100, 100, true );

		register_nav_menus( array(
			'primary' => __( 'Primary Navigation', $this->text_domain ),
			'secondary' => __( 'Secondary Navigation', $this->text_domain ),
		));
	}

	/**
	 * Load page stylesheets and scripts
	 */
	public function assets(){
		if ( !is_admin() ) {

			wp_enqueue_style( 'style', get_stylesheet_uri(), [], '1.0.3', 'all' );

			wp_deregister_script( 'jquery' );

			wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', [], '1.0.0',
				true );

			wp_enqueue_style( 'flickity-css', '//unpkg.com/flickity@2/dist/flickity.min.css' );

			wp_enqueue_style( 'fwk_google_fonts', '//fonts.googleapis.com/css?family=IBM+Plex+Sans:400,500,600');

			wp_enqueue_script( 'headroom', get_template_directory_uri() . '/assets/js/headroom.min.js', [], '1.0.0', true);

			wp_enqueue_script( 'app', get_template_directory_uri() . '/assets/js/app.js', ['headroom'], '1.0.3', true );

			/** Localize the script with new data */
			$local_data = [
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'map_icon' => [
					'desktop' => get_template_directory_uri().'/assets/images/map/xynomix-map-pin.png'
				],
			];

			wp_localize_script( 'app', 'app', $local_data );
		}
	}

	/**
	 * Register theme widgets
	 */
	public function widgets() {

		register_sidebar( [
			'name' => __('Widget Area 1', $this->text_domain ),
			'description' => __('Description for this widget-area...', $this->text_domain ),
			'id' => 'widget-area-1',
			'before_widget' => '<div id="%1$s" class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		]);

		register_sidebar( [
			'name' => __('Widget Area 2', $this->text_domain ),
			'description' => __('Description for this widget-area...', $this->text_domain ),
			'id' => 'widget-area-2',
			'before_widget' => '<div id="%1$s" class="%2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		]);
	}

	/**
	 * Add page slug to body class
	 *
	 * @param $classes
	 *
	 * @return array
	 */
	public function add_slug_to_body_class( $classes ) {
		global $post;

		if ( is_home() ) {
			$key = array_search('blog', $classes);
			if ($key > -1) {
				unset($classes[$key]);
			}
		} elseif ( is_page() ) {
			$classes[] = sanitize_html_class($post->post_name);
		} elseif ( is_singular() ) {
			$classes[] = sanitize_html_class($post->post_name);
		}

		return $classes;
	}

	/**
	 * Show/Hide admin bar when user is logged in
	 *
	 * @param $hide
	 *
	 * @return $this
	 */
	public function hide_admin_bar( $hide ) {
		if ( $hide == true ) {
			add_filter('show_admin_bar', '__return_false');
		}

		return $this;
	}

	/**
	 * Remove thumbnail width and height dimensions
	 * that prevent fluid images in the_thumbnail
	 *
	 * @param $html
	 * @return mixed
	 */
	public function remove_thumbnail_dimensions( $html ) {
		$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
		return $html;
	}

	/**
	 * Allow svg uploads
	 *
	 * @param  $mime_types
	 * @return array|string
	 */
	public function allowed_mime_types( $mime_types ) {
		$mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
		return $mime_types;
	}

	/**
	 * Modify the main query object
	 *
	 * @param $query
	 */
	public function modify_main_query( $query ) {
		if ( $query->is_home() && $query->is_main_query() ) { // Run only on the blog page
			$query->query_vars['posts_per_page'] = 10; // Show only 10 posts on the homepage only
			$query->query_vars['order'] = 'DESC';
			$query->query_vars['orderby'] = 'ID';
		}
	}

	/**
	 * does it require an options page
	 *
	 * @param string $name
	 *
	 * @return $this
	 * @internal param $query
	 *
	 */
	public function options_page($name = 'Site Options') {

		if ( function_exists('acf_add_options_page') ) {
			acf_add_options_page([
				'page_title' => $name
			]);
		}

		return $this;
	}

	/**
	 * Apply google map api key
	 *
	 * @internal param $key
	 */
	public function set_acf_google_apikey($key) {
		add_filter( 'acf/settings/google_api_key', function ($key) {
			return $key;
		});

		return $this;
	}

	public function xynomix_acf_init() {
		acf_update_setting('google_api_key','AIzaSyCa4t_URP4Ed0Dn3YE_C6mJaXQydoo2BcY');
	}

	public function wpcf7_tag_cleanup($content) {
		$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

		return $content;
	}

	/**
	 *
	 * Remove default wysiwyg & comment modules
	 *
	 */
	public function remove_extras() {

		// Hide the editor section on all pages
		remove_post_type_support('page', 'editor');
		remove_post_type_support('post', 'editor');
		remove_post_type_support('service', 'editor');
		remove_post_type_support('case-studies', 'editor');
		remove_post_type_support('team-members', 'editor');
		remove_post_type_support('testimonials', 'editor');
		remove_post_type_support('technologies', 'editor');
		remove_post_type_support('job-vacancies', 'editor');
		remove_post_type_support('partners', 'editor');
		remove_post_type_support('news', 'editor');
		remove_post_type_support('faqs', 'editor');
		remove_post_type_support('events', 'editor');
		
		// Hide the comments section on all pages
		remove_post_type_support('page', 'comments');
		remove_post_type_support('post', 'comments');
		remove_post_type_support('service', 'comments');
		remove_post_type_support('case-studies', 'comments');
		remove_post_type_support('team-members', 'comments');
		remove_post_type_support('testimonials', 'comments');
		remove_post_type_support('technologies', 'comments');
		remove_post_type_support('job-vacancies', 'comments');
		remove_post_type_support('partners', 'comments');
		remove_post_type_support('news', 'comments');
		remove_post_type_support('faqs', 'comments');
		remove_post_type_support('events', 'comments');
	}

	public function contact_form_7_dynamic_field($form_tag) {

        $queried_object = get_queried_object();

        if ( $form_tag['name'] == 'job' && $queried_object ) {
            $form_tag['values'][] = $queried_object->post_title;
        }

        return $form_tag;
	}
	
	public function api_templates() {
		if ( is_page_template( 'page-events-template.php' ) ) {
			echo View::render(get_template_directory().'/src/API/Events/Templates/Events.php');
			echo View::render(get_template_directory().'/src/API/Events/Templates/NoEvents.php');
		}
	}
}
