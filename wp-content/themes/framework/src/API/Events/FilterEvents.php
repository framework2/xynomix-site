<?php

namespace TheFramework\API\Events;

if ( !defined('ABSPATH') ) exit;

use WP_Query;
use \TheFramework\View;
use WP_REST_Response;

class FilterEvents {

	public static function request($request) {
		$params   = $request->get_params();
		$category = (isset($params['category']) && !empty($params['category'])) ? $params['category'] : null;
		$date = (isset($params['date']) && !empty($params['date'])) ? $params['date'] : null;

		if (!is_null($date)) {
			$date = json_decode($date);
		}

		$response = self::fetchData($category, $date);
		
		return new WP_REST_Response($response);
	}

	public static function fetchData($category = null, $date = null) {

		$args = [
			'post_type'   => 'events',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		];
		
		if (!is_null($date)) {
			$dateArgs = [
				'month'  => $date->month,
				'year' => $date->year,
			];

			$args = array_merge($args, $dateArgs);
		}

		if (!is_null($category)) {
			$args['tax_query'] = [
				[
					'taxonomy' => 'event-categories',
					'field'    => 'slug',
					'terms'    => [$category],
				]
			];
		}
    
		$the_query = new WP_Query( $args );

		$events = [];

		if ( $the_query->have_posts() ) {
			while ( $the_query->have_posts() ) { 
				$the_query->the_post();

				array_push($events, [
					'title' =>  get_the_title(),
                    'image' =>  get_the_post_thumbnail_url(),
                    'excerpt' => get_field('Details')['excerpt'],
					'link' => get_permalink(),
					'date' => get_field('event_details')['date'],
					'time' => get_field('event_details')['time'],
					'categories' =>  wp_get_post_terms(get_the_ID(), 'event-categories')
				]);
			}
			wp_reset_postdata();
		}

		return (count($events) > 0) ? ['success' => true, 'events' => $events] : ['success' => false];

	}
}