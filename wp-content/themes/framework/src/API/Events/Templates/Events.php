<script type="text/template" class="template" id="FilteredEvents">

	<% _.forEach(events, function(event) { %>
        <div class="c-checkerboard__item small:mb-12 xxlarge:mb-16">
            <div class="row-inner large:flex items-center justify-between c-checkerboard__item-inner">
                <div class="small-12 large-6 column small:mb-6 xxlarge:mb-0">
                    <div class="pt-100% bg-center relative bg-cover bg-no-repeat w-full rounded-large c-checkerboard__image" style="background-image:url('<%- event.image %>');" data-animate="fade-up">
                    </div>
                </div>
                <div class="small-12 large-6 xxlarge-5 column">
                    <h2 class="xynomix-heading text-black small:mb-4 xxlarge:mb-5" data-animate="fade-up"><%- event.title %></h2>

                    <div class="small:mb-2 xxlarge:mb-3 w-full">      
                        <span class="xynomix-copy-l mb-2 text-xynomix-mako block" data-animate="fade-up">Date: <%- event.date %></span>   
                        <span class="xynomix-copy-l mb-2 text-xynomix-mako block" data-animate="fade-up">Time: <%- event.time %></span>
                    </div>
                    
                    <% if (event.categories.length > 0) { %>
                        <span class="xynomix-copy mb-2 text-xynomix-raven block" data-animate="fade-up">Category</span>
                        <ul class="list-reset small:mb-4 xxlarge:mb-5 c-checkerboard__categories flex flex-wrap items-center">
                            <% _.forEach(event.categories, function(category) { %>
                                <li class="c-checkerboard__categories-item text-xynomix-red-ribbon xynomix-copy px-3 py-1" data-animate="fade-up"><%- category.name %></li>
                            <% }); %>
                        </ul>
                    <% } %>

                    <p class="xynomix-copy-m text-xynomix-raven small:mb-6" data-animate="fade-up"><%- event.excerpt %></p>

                    <div data-animate="fade-up">
                        <a href="<%- event.link %>" class="btn btn--secondary">More information</a>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
	<% }); %>
</script>