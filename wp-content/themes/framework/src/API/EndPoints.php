<?php

namespace TheFramework\API;

if ( !defined('ABSPATH') ) exit;

class EndPoints {

	protected $endpoints = [];

	public function __construct() {
		add_action('rest_api_init', [$this, 'register_rest_routes']);
	}

	/**
	 * Loop through and register individual routes
	 */
	public function register_rest_routes() {
		foreach ( $this->endpoints as $api ) {
			register_rest_route( $api['namespace'], $api['route'], $api['args'], $api['override'] );
		}
	}

	/**
	 * @param $namespace
	 * @param $route
	 * @param array $args
	 * @param bool $override
	 */
	public function endpoint($namespace, $route, $args = array(), $override = false) {
		array_push($this->endpoints, [
			'namespace' => $namespace,
			'route'     => $route,
			'args'      => $args,
			'override'  => $override
		]);
	}
}