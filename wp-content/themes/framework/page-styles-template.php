<?php 
    /* Template Name: Styles */
?>

<?php get_header(); ?>

    <h1 class="xynomix-heading mb-3">Xynomix Heading</h1>
    <h2 class="xynomix-heading-s mb-3">Xynomix Heading Small</h2>
    <p class="xynomix-copy-l mb-3">Xynomix Copy Large</p>
    <p class="xynomix-copy mb-3">Xynomix Copy</p>
    <p class="xynomix-copy-s mb-3">Xynomix Copy Small</p>
    <p class="xynomix-copy-xs mb-3">Xynomix Copy Extra Small</p>

    <?php Framework::btnPrimary('See all events', null); ?>
    <?php Framework::btnPrimaryAlt('See all events', null); ?>
    <?php Framework::btnPrimaryAlt2('See all events', null); ?>
    <?php Framework::btnSecondary('See all events', null); ?>
    <?php Framework::btnSecondaryAlt('See all events', null); ?>
    <?php Framework::btnArrow('See all events', null); ?>
    <?php Framework::btnCircle(null, null); ?>
    <?php Framework::btnCircleAlt(null, null); ?>
    <?php Framework::btnCircleAlt2(null, null); ?>
    <?php Framework::btnBio('bio', null); ?>

<?php get_footer(); ?>