<?php
require_once( get_template_directory().'/vendor/autoload.php');
require_once( get_template_directory().'/src/helpers.php' );

use TheFramework\Theme;
use TheFramework\CPT\CustomPostType;
use TheFramework\CT\CustomTaxonomy;
use TheFramework\API\EndPoints;
use TheFramework\API\Events\FilterEvents;
use TheFramework\Woocommerce\Woocommerce;

class Framework extends Theme {};

$theme              = new Framework();
$routes             = new EndPoints();
$custom_taxonomy    = new CustomTaxonomy();
$custom_post_types  = new CustomPostType();
$woocommerce        = new Woocommerce();

$custom_post_types->create('service', 'Service', 'Services', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('partners', 'Partner', 'Partners', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);


$custom_post_types->create('case-studies', 'Case study', 'Case studies', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('technologies', 'Technology', 'Technologies', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('testimonials', 'Testimonial', 'Testimonials', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'publicly_queryable' => false,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('team-members', 'Team member', 'Team members', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('job-vacancies', 'Job', 'Job vacancies', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('news', 'News', 'News', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);


$custom_taxonomy->add('news-categories', 'News category', 'News categories', 'news', [
    'hierarchical'  => true,
    'public'        => true
]);

$custom_post_types->create('faqs', 'FAQ', 'FAQs', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'publicly_queryable' => false,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_post_types->create('events', 'Event', 'Events', [
    'has_archive'   => false,
    'menu_icon'     => 'dashicons-editor-ol',
    'menu_position' => 15,
    'show_in_rest'  => true,
    'supports'      => [ 'title', 'thumbnail', 'editor']
]);

$custom_taxonomy->add('event-categories', 'Event category', 'Event categories', 'events', [
    'hierarchical'  => true,
    'public'        => true
]);

$routes->endpoint($theme->namespace, 'filtered-events/event', [
        'methods' => 'GET',
        'callback' => [FilterEvents::class, 'request']
    ]
);

$theme->options_page()->hide_admin_bar(true);