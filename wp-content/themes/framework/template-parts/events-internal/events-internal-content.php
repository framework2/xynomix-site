<?php 
    $content = get_field('content');
    $eventDetails = get_field('event_details');
?>

<?php if ($content) : ?>
	<section class="c-events-internal-content l-container small:mb-10 xxlarge:mb-20">
		<div class="small-12 large-12 xxlarge:10 mx-auto">
			<div class="row-inner clearfix lg:flex large:flex-row-reverse">

				<div class="column small-12 large:hidden small:mb-8">
					<div>
						<h3 class="xynomix-copy-l font-medium text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up">Date &amp; time</h3>
						<?php if (!empty($eventDetails['start_date'])) : ?>
							<p class="xynomix-copy text-xynomix-raven mb-0" data-animate="fade-up">Start Date - <?php echo $eventDetails['start_date']?></p>
						<?php endif; ?>
						<?php if (!empty($eventDetails['start_time'])) : ?>
							<p class="xynomix-copy text-xynomix-raven mb-2" data-animate="fade-up">Start Time - <?php echo $eventDetails['start_time']?></p>
						<?php endif; ?>
						<?php if (!empty($eventDetails['end_date'])) : ?>
							<p class="xynomix-copy text-xynomix-raven mb-0" data-animate="fade-up">End Date - <?php echo $eventDetails['end_date']?></p>
						<?php endif; ?>
						<?php if (!empty($eventDetails['end_time'])) : ?>
							<p class="xynomix-copy text-xynomix-raven mb-0" data-animate="fade-up">End Time - <?php echo $eventDetails['end_time']?></p>      
						<?php endif; ?>
					</div>
				</div>

				<div class="column small-12 large-6 super-5 max-w-copy-cap-l mr-auto super:mx-auto">
					<?php if (isset($content['heading'])) : ?>
						<h2 class="xynomix-copy-l font-medium text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $content['heading']; ?></h2>
					<?php endif; ?>
					<?php if (isset($content['copy'])) : ?>
						<div class="xynomix-content text-xynomix-raven" data-animate="fade-up">
							<?php echo $content['copy']; ?>
						</div>
					<?php endif; ?>    
				</div>
			
				<div class="column large-6 xlarge-5 large:mb-0">
					<div class="small:hidden large:block mb-9">
						<h3 class="xynomix-copy-l font-medium text-xynomix-mako mb-4" data-animate="fade-up">Date &amp; time</h3>
						<?php if (!empty($eventDetails['timings'])) : ?>
							<p class="xynomix-copy text-xynomix-raven" data-animate="fade-up"><?= $eventDetails['timings']?></p>    
						<?php endif; ?>
					</div>
					<?php if (!empty($eventDetails['event_address'])) : ?>
						<div class="small:mt-4 large:mt-12 small:hidden large:block mb-10">
							<h3 class="xynomix-copy-l font-medium text-xynomix-mako mb-4" data-animate="fade-up">Location</h3>

							<ul class="list-reset">
								<?php foreach ($eventDetails['event_address'] as $key => $addressLine) : ?>
									<li class="xynomix-copy text-xynomix-raven mb-0" data-animate="fade-up"><?php echo $addressLine['address_line']; ?></li>
								<?php endforeach; ?>
							</ul>     
						</div>
					<?php endif; ?>
					<?php if (!empty(get_field('form_code'))) : ?>
						<h2 class="xynomix-heading text-xynomix-mako mb-7 xlarge:mb-8" data-animate="fade-up"><?= get_field('form_heading'); ?></h2>
						<div class="c-contact-cta__form c-contact__event" data-animate="fade-up">
								<?php echo do_shortcode(get_field('form_code')); ?>
						</div>
					<?php endif; ?> 
				</div>

			</div>
		</div>
	</section>
<?php endif; ?>