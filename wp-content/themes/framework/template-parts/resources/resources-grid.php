<?php 
    $args = [
        'post_type' => 'News',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
    ];

    $paginationIds = [];
    $the_query = new WP_Query($args);
    $chunked = array_chunk($the_query->posts, 8);
?>
<section class="c-resources-grid l-container overflow-hidden small:mb-10 xxlarge:mb-20">
    <div class="c-resources-grid__intro small:mb-6 xxlarge:mb-12">
        <h2 class="xynomix-heading text-xynomix-mako text-center" data-animate="fade-up">News &amp; updates</h2>
    </div>

    <div class="c-resources-grid__slider row-inner is-hidden">
        <?php foreach ($chunked as $key => $chunk) : ?>

            <?php 
                array_push($paginationIds, $key);
            ?>

            <div class="c-resources-grid__slide flex flex-wrap w-full">
                <?php foreach ($chunk as $key2 => $post) : ?>
                    <?php 
                        $title = $post->post_title;
                        $excerpt = get_field('Details', $post->ID)['excerpt'];
                        $link = get_permalink($post->ID);
                        $date = get_the_date('d/m/Y',$post->ID);
                    ?>
                    <div class="column small-12 large-6 xxlarge-3 min-h-full mb-4" data-animate="fade-up">
                        <a href="<?php echo $link; ?>" class="c-resources-grid__slide-item p-6 bg-xynomix-albaster w-full block h-full flex flex-col">
                            <h3 class="xynomix-copy-m mb-1 text-xynomix-red-ribbon"><?php echo $title; ?></h3>

                            <p class="xynomix-copy text-xynomix-mako mb-3"><?php echo $date; ?></p>

                            <p class="xynomix-copy text-xynomix-raven mb-5"><?php echo $excerpt; ?></p>

                            <?php Framework::btnArrow('Read article', null, 'mt-auto', 'span'); ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>                                
        <div class="clearfix"></div>
        <?php wp_reset_postdata(); ?>
    </div>

    <?php if (count($paginationIds) > 1) : ?>
        <div class="c-resources-grid__pagination flex items-center justify-center flex-wrap  small:mt-6 xxlarge:mt-8">
            <?php foreach ($paginationIds as $key => $id) : ?>
            <div class="c-resources-grid__pagination-btn-wrapper mb-2" data-animate="fade-up" data-slide="<?php echo $id ?>">

                <?php 
                    if ($key === 0)  {
                        Framework::btnPagination($id+1, null, 'mx-1 active', 'span');
                    } else {
                        Framework::btnPagination($id+1, null, 'mx-1', 'span');
                    }
                ?>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</section>