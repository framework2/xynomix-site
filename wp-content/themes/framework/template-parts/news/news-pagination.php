<?php 
    global $post;
    $nextPost = get_next_post();
    $prevPost = get_previous_post();
?>

<section class="c-news-pagination l-container">
    <div class="row-inner large:flex justify-center clearfix">

    <?php if ($prevPost) : ?>
        <div class="column small-12 large-6 small:my-4 large:my-0">
            <a href="<?php echo get_the_permalink($prevPost->ID);?>" class="c-news-pagination__item pt-100% bg-center bg-no-repeat bg-cover relative block rounded-large" style="background-image:url('<?php echo get_the_post_thumbnail_url($prevPost->ID); ?>');">
                <div class="absolute pin-t pin-l w-full h-full flex items-center justify-center small:p-4 large:p-10 super:p-20">
                    <div>
                        <span class="xynomix-copy-l text-white block">Previous article</span>
                        <h3 class="xynomix-heading text-white"><?php echo $prevPost->post_title; ?></h3>
                    </div>
                </div>
            </a>
        </div>
    <?php endif; ?>

    <?php if ($nextPost) : ?>
        <div class="column small-12 large-6 small:my-4 large:my-0">
            <a href="<?php echo get_the_permalink($nextPost->ID); ?>" class="c-news-pagination__item pt-100% bg-center bg-no-repeat bg-cover relative block rounded-large" style="background-image:url('<?php echo get_the_post_thumbnail_url($nextPost->ID); ?>');">
                <div class="absolute pin-t pin-l w-full h-full flex items-center justify-center small:p-4 large:p-10 super:p-20">
                    <div>
                        <span class="xynomix-copy-l text-white block">Next article</span>
                        <h3 class="xynomix-heading text-white"><?php echo $nextPost->post_title; ?></h3>
                    </div>
                </div>
            </a>
        </div>
    <?php endif; ?>    
    </div>
</section>