<?php 
    $opening = get_field('opening');
?>


<section class="c-about-intro l-container small:mb-8 xxlarge:mb-12">
    <div class="c-about-intro__copy mx-auto">
        <h2 class="xynomix-heading text-center small:mb-4 xxlarge:mb-6 text-xynomix-mako" data-animate="fade-up"><?php echo $opening['heading']; ?></h2>
        <p class="xynomix-copy-m text-center text-xynomix-raven" data-animate="fade-up"><?php echo $opening['copy']; ?></p>
    </div>
</section>