<?php 
    $intro = get_field('intro');
?>

<section class="c-public-sector-intro l-container small:mb-10 xxlarge:mb-20">
    <div class="row-inner xxlarge:flex xxlarge:justify-between">
        <div class="small-12 large-5 column small:mb-8 large:mb-0">
            <h2 class="xynomix-heading text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $intro['heading']; ?></h2>
            <p class="xynomix-copy-m text-xynomix-raven small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $intro['large_copy']; ?></p>
            <div class="xynomix-content text-xynomix-raven" data-animate="fade-up"><?php echo $intro['copy']; ?></div>
        </div>

        <div class="small-12 large-6 column">
            <div class="pt-100% rounded-large bg-center bg-cover bg-no-repeat c-public-sector-intro__image" style="background-image:url('<?php echo $intro['image']; ?>');" data-animate="fade-up"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>