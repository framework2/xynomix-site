<?php 
    $accreditations = get_field('accreditations');
?>

<section class="c-public-sector-accreditations small:py-10 xxlarge:py-20">
    <div class="l-container-l">
        <div class="c-public-sector-accreditations__intro small:mb-10 xxlarge:mb-17 mx-auto">
            <h2 class="xynomix-heading text-white text-center small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $accreditations['heading'] ?></h2>
            <p class="mx-auto text-white text-center xynomix-copy-m c-public-sector-accreditations__intro-copy" data-animate="fade-up"><?php echo $accreditations['copy']; ?></p>
        </div>

        <div class="large:flex flex-wrap">
            
            <?php foreach ($accreditations['accreditations'] as $key => $accreditation) : ?>
                
                <?php 
                    $image = $accreditation['image'];
                    $heading = $accreditation['heading'];
                ?>

                <div class="c-public-sector-accreditations__item small-12 large-6 xxlarge-3 small:mb-6 xxlarge:mb-0 small:p-7 xxlarge:py-5 xxlarge:px-10 flex flex-col" data-animate="fade-up">

                    <div class="c-public-sector-accreditations__image w-full bg-no-repeat bg-center bg-contain small:mb-3 xxlarge:mb-9" style="background-image:url('<?php echo $image; ?>');" data-animate="fade-up"></div>

                    <h3 class="text-center xynomix-copy-m text-white w-full mt-auto mb-auto" data-animate="fade-up"><?php echo $heading; ?></h3>
                </div>
            <?php endforeach; ?>
        </div>                                
        <div class="clearfix"></div>
    </div>
</section>