<?php 
    $feature_text = get_field('feature_text');
?>

<section class="c-public-sector-feature-text l-container small:mb-10 xxlarge:mb-20">
    <h2 class="text-xynomix-mako xynomix-heading small:mb-4 xxlarge:mb-6 text-center" data-animate="fade-up"><?php echo $feature_text['heading']; ?></h2>   
    <p class="xynomix-copy-m text-xynomix-raven text-center c-public-sector-feature-text__copy mx-auto" data-animate="fade-up"><?php echo $feature_text['copy']; ?></p>
</section>