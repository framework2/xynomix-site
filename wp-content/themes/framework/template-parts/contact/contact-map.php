<?php 
    $map = get_field('company_details', 'option')['map'];
?>
<section class="c-contact-map-wrapper relative" data-animate="fade-up">
     <div class="c-contact-map absolute pin-t pin-l w-full h-full" id="googleCanvas" data-lat='<?php echo $map['lat']; ?>' data-lng='<?php echo  $map['lng']; ?>' data-address='<?php echo $map['address']; ?>'></div>
</section>