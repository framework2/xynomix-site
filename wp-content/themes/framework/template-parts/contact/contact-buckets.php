<?php 
    $details = get_field('company_details', 'option');
?>

<section class="c-contact-buckets l-container small:mb-10 xxlarge:mb-20">
    <div class="small-12 xxlarge-10 mx-auto">
        <div class="row-inner large:flex flex-wrap">
            <div class="column w-full xxlarge:mb-7" data-animate="fade-up">

                <div class="rounded-large c-contact-buckets__item small:p-4 small:py-7 xxlarge:p-7 xxlarge:py-16 bg-white">
                    <div class="large-9 mx-auto">
                
                        <div class="small:mb-5 large:mb-10 medium:flex medium:items-center">
                            <div class="c-contact-buckets__icon bg-center bg-contain bg-no-repeat medium:mr-5 large:mr-16 mb-4 medium:mb-0" style="background-image: url('<?php echo $details['icon']; ?>');"></div>
                            <h2 class="xynomix-heading-s text-xynomix-mako">Get in touch</h2>
                        </div>

                        <div class="c-contact-cta__form border-b border-xynomix-raven small:pb-8 large:pb-16 small:mb-5 large:mb-12" data-animate="fade-up">
                            <?php echo do_shortcode('[contact-form-7 id="176" title="Contact CTA"]'); ?>
                        </div>

                        <div class="large-9 large:flex mx-auto">
                            <a href="tel: <?php echo $details['phone_number']; ?>" class="xynomix-copy text-xynomix-mako block mb-0 small:text-center large:text-left mr-auto small:mb-4 large:mb-0">
                            <img class="c-contact-buckets__tel" src="<?= get_theme_file_uri('/assets/images/contact/tel.png'); ?>" alt="">
                        </a>
                            <a href="mailto: <?php echo $details['email_address']; ?>" class="xynomix-copy text-xynomix-mako block small:text-center large:text-left ml-auto">
                            <img class="c-contact-buckets__email" src="<?= get_theme_file_uri('/assets/images/contact/email.png'); ?>" alt="">
                        </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>