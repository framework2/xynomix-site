<?php 
    $clients = get_field('clients');


?>

<section class="c-home-clients w-full bg-black small:mb-12 xxlarge:mb-20">
    <div class="l-container">
        <div class="c-home-clients__intro small:mb-5 xxlarge:mb-13">
            <h2 class="xynomix-heading text-white text-center small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $clients['heading']; ?></h2>
            <p class="text-center xynomix-copy-m text-white c-home-clients__intro-copy mx-auto" data-animate="fade-up"><?php echo $clients['copy']; ?></p>
        </div>
        
        <?php if (!empty($clients['logos'])) : ?>
            <div class="c-home-clients__grid flex flex-wrap row-inner">
                <?php foreach ($clients['logos'] as $key => $client) : ?>

                    <div class="c-home-clients__grid-item flex justify-center column">
                        <div class="c-home-clients__grid-icon relative bg-white p-6 flex items-center justify-center" data-animate="fade-up">
                                <img src="<?php echo $client['logo']; ?>"/>
                        </div>
                    </div>
                <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>