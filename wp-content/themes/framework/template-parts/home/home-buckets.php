<?php 
    $buckets = get_field('buckets');
?>

<section class="l-container overflow-hidden small:-mt-8 xxlarge:-mt-12 small:pb-8 xxlarge:pb-20">
    <div class="row-inner clearfix">
        <div class="xxlarge:flex justify-center c-home-buckets">
            
            <?php foreach ($buckets as $key => $bucket) : ?>

                <?php 
                    $link = get_permalink($bucket->ID);
                    $name = $bucket->post_title;
                    $icon = get_field('Details', $bucket->ID)['icon'];
                    $delay = $key * 100;
                ?>

                <div class="small-12 large-6 xxlarge-4 column min-h-full" data-animate="fade-up" data-delay="<?php echo $delay; ?>">
                    <a href="<?php echo $link; ?>" class="flex flex-col items-center bg-white px-6 pb-7 pt-14 c-home-buckets-item h-full bg-xynomix-albaster">
                        <div class="c-home-buckets-icon bg-center bg-contain bg-no-repeat small:mb-3 xxlarge:mb-11" style="background-image: url('<?php echo $icon; ?>');"></div>
                        <h3 class="text-xynomix-mako xynomix-copy-l text-center mb-8"><?php echo $name; ?></h3>
                    
                        <?php Framework::btnArrow('Learn more' , null, 'mt-auto block', 'span'); ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>