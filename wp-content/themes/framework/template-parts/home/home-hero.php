<?php
    $image = get_the_post_thumbnail_url();
    $hero = get_field('home_hero');
?>
<section class="overflow-hidden w-full">
    <div class="c-hero c-hero--home bg-center bg-cover bg-no-repeat flex justify-center items-center" style="background-image:url('<?php echo $image; ?>');">
        <div class="relative w-full">
            <?php get_template_part('template-parts/sitewide/hero-social'); ?>
            
            <div class="c-hero__content flex flex-col items-center l-container">
                <div class="c-hero__content-logo relative small:mb-6 xxlarge:mb-8">
                    <?php // Framework::svgIcon('xynomix-logo-full', '0 0 384 181'); ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/svgs/xynomix-full.svg"/>
                </div>
            
                <?php 
                    if (count($hero['hero_slides']) > 1) {
                        $class = 'is-multiple';
                    } else {
                        $class = 'is-single';
                    }
                ?>

                <div class="c-hero__slider w-full is-hidden <?php echo $class; ?>">
                    <?php foreach ($hero['hero_slides'] as $key => $slide) : ?>
                        <div class="c-hero__slider-item w-full min-h-full flex flex-col justify-center items-center">
                            <h2 class="text-white xynomix-heading-s text-center small:mb-5 xxlarge:mb-7"><?php echo $slide['heading']; ?></h2>
                            <p class="c-hero__slider-copy text-white xynomix-copy-l text-center mx-auto"><?php echo $slide['copy']; ?></p>

                            <?php if (!empty($slide['button_text']) && !empty($slide['button_link'])) : ?> 
                                <div class="flex justify-center">
                                    <?php Framework::btnPrimaryAlt3($slide['button_text'], $slide['button_link'], 'small:mt-5 xxlarge:mt-7'); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>             


            </div>
        </div>
    </div>
</section>