<?php 
    $split = get_field('split');
?>

<section class="c-about-split l-container small:mb-7 xxlarge:mb-0">
    <div class="flex small:flex-col-reverse xxlarge:justify-between xxlarge:flex-row row-inner items-center">
        <div class="column small-12 xxlarge-5">
            <p class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $split['large_copy']; ?></p>
            <div class="xynomix-content text-xynomix-raven" data-animate="fade-up"><?php echo $split['copy']; ?></p></div>
        </div>
        <div class="column small-12 xxlarge-6 small:mb-10 xxlarge:mb-0">
            <div class="c-about-split__image rounded-large pt-100% bg-center bg-cover bg-no-repeat" style="background-image:url('<?php echo $split['image']; ?>');" data-animate="fade-up">
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>