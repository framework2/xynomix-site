<?php 
    $args = [
        'post_type' => 'job-vacancies',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
    ];

    $the_query = new WP_Query($args);

    $careers = get_field('careers');
?>

<?php if ( $the_query->have_posts() ) : ?>
    <section class="c-about-careers small:mb-10 xxlarge:mb-16 l-container">
            <div class="c-about-careers__intro mx-auto small:mb-8 xxlarge:mb-13">
                <h2 class="xynomix-heading text-xynomix-mako text-center small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $careers['heading']; ?></h2>
                <p class="xynomix-copy-m text-xynomix-raven text-center" data-animate="fade-up"><?php echo $careers['copy']; ?></p>
            </div>      
        
        <div>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                
                <?php 
                    $title = get_the_title();
                    $excerpt = get_field('Details')['excerpt'];
                    $link = get_the_permalink();

                ?>

                <div class="c-about-careers__item overflow-hidden">
                    <div class="c-about-careers__title-bar flex items-center justify-between small:py-4 small:px-3 xxlarge:p-4">
                        <h3 class="xynomix-copy-l text-xynomix-red-ribbon mr-3"><?php echo $title; ?></h3>

                        <?php Framework::btnAccordion(null, null, null, 'span'); ?>
                        
                    </div>
                    <div class="c-about-careers__overview overflow-hidden">
                        <div class="small-10 mx-auto mt-3 small:mb-10 xxlarge:mb-15">
                            <div class="c-about-careers__overview-content">
                                <h4 class="xynomix-copy-m text-xynomix-mako mb-4">Job overview</h4>

                                <p class="xynomix-copy text-xynomix-raven mb-7"><?php echo $excerpt; ?></p>

                                <?php Framework::btnSecondary('Learn more', $link); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>