<?php 
    $args = [
        'post_type' => 'team-members',
        'posts_per_page' => '-1',
        'post_status' => 'publish',
                'orderby' => 'menu_order',
        'order' => 'ASC',
    ];

    $the_query = new WP_Query($args);

    $team = get_field('team');
?>

<?php if ( $the_query->have_posts() ) : ?> 
    <div class="overflow-hidden small:mb-11 xxlarge:-mt-5 xxlarge:mb-13"> 
        <section class="c-about-team overflow-hidden">
            <div class="l-container">
                <div class="c-about-team__intro mx-auto small:mb-8 xxlarge:mb-16">
                    <h2 class="xynomix-heading text-xynomix-mako small:mb-4 xxlarge:mb-6 text-center" data-animate="fade-up"><?php echo $team['heading']; ?></h2>
                    <p class="xynomix-copy-m text-xynomix-raven text-center" data-animate="fade-up"><?php echo $team['copy']; ?></p>
                </div>
                <div class="row-inner w-full">
                    <div class="xxlarge:flex flex-wrap c-about-team__grid small:mb-4 xxlarge:mb-0">
                        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                            <?php 
                                $name = get_the_title();
                                $image = get_the_post_thumbnail_url();
                                $position = get_field('position');
                                $bio = get_field('bio');
                                $social = get_field('social');
                            ?>

                            <div class="column small-12 large-6 xxlarge-4 small:mb-4 xxlarge:mb-14" data-animate="fade-up">
                                <div class="c-about-team__item relative rounded-large bg-center bg-cover bg-no-repeat mb-5" style="background-image:url('<?php echo $image; ?>');">
                                    <?php Framework::btnBio('Bio', null, 'pin-t pin-r m-3 z-10', 'span'); ?>
                                    <div class="absolute pin-t pin-l w-full h-full flex flex-col c-about-team__bio overflow-hidden rounded-large small:p-8 pt-15">
                                        <div class="w-full">
                                            <p class="xynomix-copy-m text-white mb-4"><?php echo $position; ?></p>
                                            <p class="text-white xynomix-copy"><?php echo $bio; ?></p>
                                        </div>
                                        <div class="flex items-center mt-auto">
                                            <?php if ($social['linkedin']) : ?>
                                                <a href="<?php echo $social['linkedin']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-linkedin text-white relative">
                                                    <?php Framework::svgIcon('linkedin', '0 0 17 17'); ?>
                                                </a>
                                            <?php endif; ?>
                                            <?php if ($social['google_plus']) : ?>
                                                <a href="<?php echo $social['google_plus']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-googleplus text-white relative">
                                                    <?php Framework::svgIcon('googleplus', '0 0 25 16'); ?>
                                                </a>
                                            <?php endif; ?>
                                            <?php if ($social['twitter']) : ?>
                                                <a href="<?php echo $social['twitter']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-twitter text-white relative">
                                                    <?php Framework::svgIcon('twitter', '0 0 19 15'); ?>
                                                </a>
                                            <?php endif; ?>
                                            <?php if ($social['facebook']) : ?>
                                                <a href="<?php echo $social['facebook']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-facebook text-white relative">
                                                    <?php Framework::svgIcon('facebook', '0 0 18 16'); ?>
                                                </a>
                                            <?php endif; ?>
                                        </div>                                    
                                    </div>
                                </div>

                                <h3 class="text-center text-xynomix-red-ribbon small:mb-3 xxlarge:mb-2 xynomix-copy-l" data-animate="fade-up"><?php echo $name; ?></h3>

                                <h4 class="xynomix-copy text-center text-xynomix-raven" data-animate="fade-up"><?php echo $position; ?></h4>
                                
                            </div>
                        
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php wp_reset_postdata(); ?>
<?php endif; ?>