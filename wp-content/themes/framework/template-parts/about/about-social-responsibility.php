<?php 
    $social_responsibility = get_field('social_responsibility');
?>

<section class="l-container-l relative z-10 small:mb-10 xxlarge:mb-20" data-animate="fade-up">
    <div class="c-about-social-responsibility rounded-large small:py-12 xxlarge:py-20">
        <div class="l-container">
            <div class="row-inner">
                <div class="column small-12 small:mb-4 xxlarge:mb-6">
                    <div class="xxlarge-6">
                        <h2 class="xynomix-heading text-white small:text-center xxlarge:text-left" data-animate="fade-up"><?php echo $social_responsibility['heading']; ?></h2>
                    </div>
                </div>

                <div class="xxlarge:flex w-full clearfix">
                    <div class="column small-12 xxlarge-6 small:mb-4 xxlarge:mb-6">
                        <h2 class="xynomix-copy-l text-white small:text-center xxlarge:text-left" data-animate="fade-up"><?php echo $social_responsibility['large_copy']; ?></h2>
                    </div>
                    <div class="column small-12 xxlarge-6 small:mb-4 xxlarge:mb-6">
                        <div class="xynomix-content text-white small:text-center xxlarge:text-left" data-animate="fade-up"><?php echo $social_responsibility['copy']; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>