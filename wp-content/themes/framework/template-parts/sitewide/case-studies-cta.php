<?php 
    $case_studies = get_field('featured_case_studies', 'option');  
    $related = get_field('related');

    $heading = 'Featured case studies';
    $class = '';
    
    if ($related) {
        $case_studies = $related;
        $heading = 'Related case studies';
    }

    if (count($case_studies) === 1) {
        $class = 'no-dots';
    }
?>


<section class="c-case-studies-cta l-container overflow-hidden <?php echo $class; ?>">
    <h2 class="text-center text-xynomix-mako xynomix-heading" data-animate="fade-up"><?php echo $heading; ?></h2>
    
    <div class="flex flex-wrap items-center small:mb-4 xxlarge:mb-12 justify-between">
        <div class="column small-6 xxlarge-1 items-center small:justify-start large:justify-center
        xxlarge:justify-start row-inner c-case-studies-cta__prev-wrapper relative z-10">
            <?php if (count($case_studies) > 1) : ?>
                <?php Framework::btnCircle(null, null, 'c-case-studies-cta__prev btn--circle-left', 'span') ?>
            <?php endif; ?>
        </div>
        <div class="small-12 xxlarge-10">
            <div class="row-inner c-case-studies-cta__slider" data-animate="fade-up">
                <?php foreach ($case_studies as $key => $slide) : ?>

                    <?php 
                        $name = get_the_title($slide->ID);
                        $details = get_field('Details', $slide->ID);
                        $project_details = get_field('project_details', $slide->ID);
                        $index = $key + 1;
                        $amount = count($case_studies);
                        $image = get_the_post_thumbnail_url($slide->ID);
                        $link = get_permalink($slide->ID);
                    ?>
                    <div class="column py-8">
                        <div class="c-case-studies-cta-item xxlarge:flex w-full">
                            <div class="small-12 xxlarge-6 bg-xynomix-albaster xxlarge:px-11 xxlarge:pt-9 small:px-8 small:py-8 xxlarge:pb-16">
                                <h4 class="xynomix-heading-s text-xynomix-mako small:mb-7 xxlarge:mb-13">0<?php echo $index; ?>/<span class="text-xynomix-alto">0<?php echo $amount; ?></span></h4>
                                <h3 class="text-xynomix-mako xynomix-copy-l font-semibold mb-2"><?php echo $name; ?></h3>
                                <p class="xynomix-copy-m text-xynomix-mako mb-5"><?php echo $project_details['company_name']; ?></p>
                                <p class="xynomix-copy-m text-xynomix-raven mb-6"><?php echo $details['excerpt']; ?></p>
                                    <?php Framework::btnArrow('Explore case study', $link); ?>
                            </div>
                            <div class="small-12 xxlarge-6 bg-center bg-cover bg-no-repeat" style="background-image:url('<?php echo $image; ?>');"></div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="column small-6 xxlarge-1 items-center small:justify-end large:justify-center xxlarge:justify-end row-inner c-case-studies-cta__next-wrapper relative z-10">
        <?php if (count($case_studies) > 1) : ?>    
            <?php Framework::btnCircle(null, null, 'c-case-studies-cta__next', 'span') ?>
        <?php endif; ?>
        </div>
    </div>

    <div class="flex items-center justify-center" data-animate="fade-in">
        <?php Framework::btnSecondary('View all Case Studies', home_url('/case-studies')); ?>
    </div>
</section>