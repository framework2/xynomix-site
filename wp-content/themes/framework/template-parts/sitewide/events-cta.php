<?php 
    $eventsCTA = get_field('events_cta', 'options');
?>

<section class="l-container">
    <div class="c-events-cta bg-center bg-cover bg-no-repeat relative rounded-large" style="background-image:url('<?php echo $eventsCTA['image']; ?>');" data-animate="fade-up">
        <div class="absolute h-full w-full pin-t pin-l flex items-center flex-col justify-center p-12"> 
                <h2 class="xynomix-heading text-white text-center small:mb-4 xxlarge:mb-8" data-animate="fade-up"><?php echo $eventsCTA['heading']; ?></h2>

                <div data-animate="fade-up">
                    <?php Framework::btnSecondaryAlt2($eventsCTA['button_text'], $eventsCTA['button_link']); ?>
                </div>
        </div>
    </div>
</section>