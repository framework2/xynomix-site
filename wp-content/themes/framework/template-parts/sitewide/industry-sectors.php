<?php 
    $industry_sectors = get_field('industry_sectors', 'option');
?>

<div class="overflow-hidden">
    <section class="c-industry-sectors">
        <div class="l-container">
            <h2 class="xynomix-heading text-white text-center mb-12" data-animate="fade-up"><?php echo  $industry_sectors['heading']; ?></h2>
            <div class="flex flex-wrap items-center justify-center clearfix">

                <div class="column small-6 xxlarge-1 items-center small:justify-start large:justify-center xxlarge:justify-start row-inner c-industry-sectors__prev-wrapper relative z-10">
                    <?php Framework::btnCircleAlt2(null, null, 'c-industry-sectors__prev btn--circle-left', 'span');  ?>
                </div>

                <div class="row-inner small-12 large-10 column c-industry-sectors__slider">
                    <?php foreach ($industry_sectors['slides'] as $key => $slide) : ?>
                        <div class="column small-12 flex-col flex items-center min-h-full justify-center">
                            <h3 class="xynomix-copy-l text-white text-center mb-4 w-full" data-animate="fade-up"><?php echo $slide['heading']; ?></h3>
                            <p class="xynomix-copy-m text-white text-center w-full c-public-sector-feature-text__copy" data-animate="fade-up"><?php echo $slide['copy']; ?></p>
                        </div>
                    <?php endforeach; ?>     
                </div>

                <div class="column small-6 xxlarge-1 items-center small:justify-end large:justify-center xxlarge:justify-end row-inner c-industry-sectors__next-wrapper relative z-10">
                    <?php Framework::btnCircleAlt2(null, null, 'c-industry-sectors__next', 'span');  ?>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>
</div>