
<?php 

    if (empty($details['heading']) && empty($details['copy']) ) {
        $details = get_field('services_cta', 'option');
    }

    $args = [
        'post_type' => 'service',
        'post_status' => 'publish',
        'posts_per_page' => '-1',
        'orderby' => 'menu_order',
        'order' => 'ASC',
    ];
    
    $the_query = new WP_Query($args);
?>

<?php if ( $the_query->have_posts() ) : ?>
    <section class="c-services l-container">
        <div class="c-services__intro mx-auto small:mb-8 xxlarge:mb-10">
            <?php if (!empty($details['heading'])) : ?>
                <h2 class="text-xynomix-mako xynomix-heading text-center small:mb-4 xxlarge:mb-6" data-animate='fade-up'><?php echo $details['heading']; ?></h2>
            <?php endif; ?>
            <?php if (!empty($details['copy'])) : ?>
                <p class="text-xynomix-mako xynomix-copy-m text-center  mx-auto" data-animate='fade-up'><?php echo $details['copy']; ?></p>
            <?php endif; ?>
        </div>

        <div class="c-services__grid">
            <div class="row-inner flex flex-wrap">
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php 
                        $link = get_the_permalink();
                        $name = get_the_title();
                        $icon = get_field('Details')['icon'];
                    ?>

                    <div class="column small-12 large-6 xxlarge-3 min-h-full mb-4" data-animate="fade-up">
                        <a href="<?php echo $link; ?>" class="small:p-3 xxlarge:p-5 flex flex-col items-center c-services__grid-item h-full bg-xynomix-albaster">
                            <div class="c-services__icon bg-center bg-no-repeat bg-contain mb-4" style="background-image: url('<?php echo $icon; ?>');"></div>

                            <h3 class="xynomix-copy-m mb-3 text-center mt-auto text-xynomix-mako w-full"><?php echo $name; ?></h3>

                            <?php Framework::btnArrow('Learn more', null, 'mt-auto', 'span'); ?>
                        </a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>