<?php 
    $details =  get_field('company_details', 'option');
?>

<?php if (is_page_template('page-contact-template.php')) : ?>
    <div class="overflow-hidden">
        <section class="c-contact-cta">
            <div class="l-container">
                <div class="xxlarge-10 mx-auto">
                    <div class="large:flex row-inner justify-center">
                        <div class="column large-6 small-12">
                            <div class="c-contact-cta__form" data-animate="fade-up">
                                <?php echo do_shortcode('[contact-form-7 id="176" title="Contact CTA"]'); ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php else : ?>
    <div class="overflow-hidden">
        <section class="c-contact-cta">
            <div class="l-container">
                <div class="xxlarge-10 mx-auto">
                    <h2 class="xynomix-heading text-xynomix-mako mb-9 small:text-center large:text-left" data-animate="fade-up">Get in touch</h2>
                    <div class="large:flex row-inner">
                        <div class="column large-6 small-12 small:mb-13 large:mb-0">
                            <div class="mb-8">
                                <p class="text-xynomix-red-ribbon xynomix-copy-m mb-2 small:text-center large:text-left" data-animate="fade-up">Customer Services</p>
                                <a href="tel: <?php echo $details['phone_number']; ?>" class="text-xynomix-raven xynomix-copy-m block small:text-center large:text-left" data-animate="fade-up">
                                    <img class="cta-contact-image" src="<?php echo get_theme_file_uri('assets/images/contact/0345-222-9600.png'); ?>">
                                </a>
                                <a href="mailto: <?php echo $details['email_address']; ?>" class="text-xynomix-raven xynomix-copy-m block small:text-center large:text-left" data-animate="fade-up">
                                <img class="cta-contact-image cta-contact-image--email" src="<?php echo get_theme_file_uri('assets/images/contact/info@xynomix.com.png'); ?>">
                            </a>
                            </div>
                        </div>
                        <div class="column large-6 small-12">
                            <div class="c-contact-cta__form" data-animate="fade-up">
                                <?php echo do_shortcode('[contact-form-7 id="176" title="Contact CTA"]'); ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php endif; ?>