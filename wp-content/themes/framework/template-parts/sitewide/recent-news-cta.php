<?php 
    $args = [
        'post_type' => 'News',
        'posts_per_page' => '5',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'order' => 'ASC',
    ];

    $the_query = new WP_Query($args);

    if ( $the_query->have_posts() ) {
        $posts = $the_query->posts;
    }

    $recentNewsCTA = get_field('recent_news_cta', 'options');

    $archive_args = array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-resources-template.php',
    );

    $archive_url = get_permalink(get_pages($archive_args)[0]->ID);    
?>
<?php if ( $the_query->have_posts() ) : ?> 
    <section class="c-recent-news-cta l-container overflow-hidden">
        <h2 class="text-center text-xynomix-mako xynomix-heading small:mb-6 xxlarge:mb-8" data-animate="fade-up"><?php echo $recentNewsCTA['heading']; ?></h2>
         
        <div class="small-12 small:mb-4 xxlarge:mb-12 clearfix">
            <div class="row-inner flex small:flex-col-reverse xxlarge:flex-row">
                <div class="c-recent-news-cta__grid small-12 xxlarge-6">
                    <div class="large:flex flex-wrap c-recent-news-cta__grid-inner">
                        <?php foreach ($posts as $key => $post) : ?>
                            <?php
                                if ($key < 1) continue; 
                                $title = $post->post_title;
                                $excerpt = get_field('Details', $post->ID)['excerpt'];
                                $link = get_permalink($post->ID);
                            ?>
                                <div class="column small-12 large-6 min-h-full mb-4" data-animate="fade-up">
                                    <a href="<?php echo $link; ?>" class="c-recent-news-cta__item p-6 bg-xynomix-albaster w-full block h-full flex flex-col">
                                        <h3 class="xynomix-copy-m mb-3 text-xynomix-red-ribbon"><?php echo $title; ?></h3>
                                        <p class="xynomix-copy text-xynomix-raven mb-5"><?php echo $excerpt; ?></p>
                                        <?php Framework::btnArrow('Read article', null, 'mt-auto', 'span'); ?>
                                    </a>
                                </div>
                        <?php endforeach; ?>
                    </div>                              
                    <div class="clearfix"></div>
                </div>

                <?php
                    $featuredTitle = $posts[0]->post_title;
                    $featuredExcerpt = get_field('Details', $posts[0]->ID)['excerpt'];
                    $featuredLink = get_permalink($posts[0]->ID);
                    $featuredImage = get_the_post_thumbnail_url($posts[0]->ID);
                ?>
                <div class="column small-12 xxlarge-6 min-h-full mb-4" data-animate="fade-up">
                    <a href="<?php echo $featuredLink; ?>" class="c-recent-news-cta__featured p-6  h-full w-full block flex flex-col bg-center bg-no-repeat bg-cover" style="background-image:url('<?php echo $featuredImage; ?>');">
                        <h3 class="xynomix-heading mb-6 text-white mt-auto"><?php echo $featuredTitle; ?></h3>
                        <p class="xynomix-copy-m text-white mb-14"><?php echo $featuredExcerpt; ?></p>
                        <?php Framework::btnArrow('Read article', null, 'mt-auto', 'span'); ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="flex items-center justify-center" data-animate="fade-in">
            <?php Framework::btnSecondary('View all articles', $archive_url); ?>
        </div>
    </section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>