<?php 
    $social = get_field('social', 'option');
?>

<div class="c-hero__social small:hidden xxlarge:flex items-center">
    <span class="xynomix-copy-xs text-white mr-2">Follow us</span>
    <div class="c-hero__social-line bg-white block mr-2"></div>
    <div class="flex items-center">
        <?php if ($social['linkedin']) : ?>
            <a href="<?php echo $social['linkedin']; ?>" target="_blank" class="c-hero__social-icon c-hero__social-linkedin text-white relative">
                <?php Framework::svgIcon('linkedin', '0 0 17 17'); ?>
            </a>
        <?php endif; ?>
        <?php if ($social['google+']) : ?>
            <a href="<?php echo $social['google+']; ?>" target="_blank" class="c-hero__social-icon c-hero__social-googleplus text-white relative">
                <?php Framework::svgIcon('googleplus', '0 0 25 16'); ?>
            </a>
        <?php endif; ?>
        <?php if ($social['twitter']) : ?>
            <a href="<?php echo $social['twitter']; ?>" target="_blank" class="c-hero__social-icon c-hero__social-twitter text-white relative">
                <?php Framework::svgIcon('twitter', '0 0 19 15'); ?>
            </a>
        <?php endif; ?>
        <?php if ($social['facebook']) : ?>
            <a href="<?php echo $social['facebook']; ?>" target="_blank" class="c-hero__social-icon c-hero__social-facebook text-white relative">
                <?php Framework::svgIcon('facebook', '0 0 18 16'); ?>
            </a>
        <?php endif; ?>
    </div>
</div>