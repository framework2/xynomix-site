<?php 

$theme_locations = get_nav_menu_locations();
$menu_object = get_term($theme_locations['primary'], 'nav_menu');

if (is_wp_error($menu_object) === false): ?>

<?php $menu = new TheFramework\Menu\Menu( $menu_object->name ); ?>

    <?php if (count($menu->items) > 0): ?>
	

        <?php 
			$name = 'l-footer__sitemap';
        ?>
	
		<ul class="<?php echo $name; ?> list-reset mb-0  inline-flex flex-col">
			<?php foreach ($menu->items as $key => $item): ?>
			
				<?php 
					$page_id     = get_queried_object_id();
					$activeClass = "";
					$submenu = $menu->get_submenu($item);
					$itemClasses = [];

					if ($submenu) {			
						foreach ($submenu as $key => $subItem) {
							if ( $page_id == $subItem->object_id){
								array_push($itemClasses, $name.'__item--childActive');
							} 
						}	
					}

					if ( $page_id == $item->object_id) {
						array_push($itemClasses, $name.'__item--active');
					} 

					$itemClasses = implode(' ', $itemClasses);
				?>

				<li class="<?php echo $name; ?>__item inline-block <?php echo $itemClasses; ?>">
					<a class="<?php echo $name; ?>__link xynomix-copy-xs text-white" href="<?php echo $item->url ?>"><?php echo $item->title; ?>
					</a>
				</li>

			<?php endforeach; ?>
		</ul>

	<?php endif; ?>

<?php endif; ?>