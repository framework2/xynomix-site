<?php if (have_rows('layout')): ?>
    <?php while (have_rows('layout') ) : the_row(); ?>
        <?php switch ( get_row_layout() ) {
            case 'list_with_image':                 
                get_template_part('template-parts/sitewide/internal-layout/list-with-image', null);
                break;  
            case 'feature_text':                 
                get_template_part('template-parts/sitewide/internal-layout/feature-text', null);
                break;  
            case 'contained_image':                 
                get_template_part('template-parts/sitewide/internal-layout/contained-image', null);
                break;  
            case 'content':                 
                get_template_part('template-parts/sitewide/internal-layout/content', null);
                break; 
            case 'centered_content':                 
                get_template_part('template-parts/sitewide/internal-layout/centered-content', null);
                break;                 
            case 'full_image':                 
                get_template_part('template-parts/sitewide/internal-layout/full-image', null);
                break;  
            case 'split_images':                 
                get_template_part('template-parts/sitewide/internal-layout/split-images', null);
                break;              
            case 'list':                 
                get_template_part('template-parts/sitewide/internal-layout/list', null);
                break;  
            case 'quote':                 
                get_template_part('template-parts/sitewide/internal-layout/quote', null);
                break;  
            case 'services_cta': ?> 
                <div class="small:mb-11 xxlarge:mb-20"> <?php 
                    $details = get_sub_field('services_cta');
                    include_once(locate_template('template-parts/sitewide/services-cta.php', null)); ?>
                </div>
                <?php break;   
            case 'cta':                 
                get_template_part('template-parts/sitewide/internal-layout/cta', null);
                break;                                
        } ?>
    <?php endwhile; ?>
<?php endif; ?>