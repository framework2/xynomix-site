<?php

    $image = get_the_post_thumbnail_url();
    $hero = get_field('hero');

    if (is_404()) {
        $image = get_field('404', 'options')['image'];
        $hero = get_field('404', 'options');
    }
?>

<section class="overflow-hidden w-full small:mb-10 xxlarge:mb-14">
    <div class="c-hero c-hero--internal bg-center bg-cover bg-no-repeat flex justify-center items-center" style="background-image:url('<?php echo $image; ?>');">
        <div class="relative w-full">
            <?php get_template_part('template-parts/sitewide/hero-social'); ?>
            <div class="c-hero__content flex flex-col items-center l-container">
                <div class="c-hero__content-inner mx-auto">
                    <h1 class="xynomix-heading text-white text-center"><?php echo $hero['heading']; ?></h1>
                    <p class="xynomix-copy-l text-white text-center mt-5"><?php echo $hero['subheading']; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>