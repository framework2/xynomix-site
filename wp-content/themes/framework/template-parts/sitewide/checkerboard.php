<?php 
$args = [
    'post_type' => $post_type,
    'posts_per_page' => '-1',
    'post_status' => 'publish',
            'orderby' => 'menu_order',
        'order' => 'ASC',
];

$the_query = new WP_Query($args);
?>

<?php if ($the_query->have_posts()) : ?> 
    <section class="c-checkerboard l-container">
        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

            <?php 
            $name = get_the_title();
            $details = get_field('Details');
            $icon = $details['icon'];
            $image = get_the_post_thumbnail_url();
            $link = get_the_permalink();
            $index = $the_query->current_post + 1;
            $amount = $the_query->post_count;
            $applications = get_field('applications');
            $btnText = 'Read more';
            $categories = wp_get_post_terms(get_the_ID(), 'event-categories');
            $eventDetails = get_field('event_details');

            switch ($post_type) {
                case 'case-studies':
                    $btnText = 'View case study';
                    break;
                case 'partners':
                    $btnText = 'Read more';
                    break;
                case 'technologies':
                    $btnText = 'Read more';
                    break;
                case 'events':
                    $btnText = 'More information';
                    break;
            }
            ?>

            <div class="c-checkerboard__item small:mb-12 xxlarge:mb-16">
                <div class="row-inner large:flex items-center justify-between c-checkerboard__item-inner">
                    <div class="small-12 large-5 column small:mb-6 xxlarge:mb-0">
                        <a href="<?php echo $link; ?>" class="pt-100% block bg-center relative bg-cover bg-no-repeat w-full rounded-large c-checkerboard__image" style="background-image:url('<?php echo $image; ?>');" data-animate="fade-up">
                    
                            <?php if ($icon) : ?>
                                <div class="flex items-center justify-center small:p-7 xxlarge:p-10 bg-xynomix-albaster absolute pin-t pin-l w-full h-full rounded-large">
                                    <div class="c-checkerboard__icon bg-center bg-contain w-full bg-no-repeat" style="background-image: url('<?php echo $icon; ?>');"></div>
                                </div>
                            <?php endif; ?>

                        </a>
                    </div>
                    <div class="small-12 large-6 xxlarge-5 column">

                        <?php if ($post_type === 'case-studies') : ?>
                            <p class="text-xynomix-mako xynomix-copy-l mb-2" data-animate="fade-up">0<?php echo $index ?>/<span class="text-xynomix-alto">0<?php echo $amount; ?></span></p>
                        <?php endif; ?>

                        <h2 class="xynomix-heading text-black small:mb-4 xxlarge:mb-5" data-animate="fade-up"><?php echo $name; ?></h2>


                        <?php if ($post_type === 'events') : ?>
                            <div class="small:mb-2 xxlarge:mb-5 w-full">
                                <?php if (!empty($eventDetails['start_date'])) : ?>
                                    <span class="xynomix-copy-l mb-2 text-xynomix-mako block" data-animate="fade-up">Start Date - <?php echo $eventDetails['start_date']; ?></span>
                                <?php endif; ?>

                                <?php if (!empty($eventDetails['end_date'])) : ?>
                                    <span class="xynomix-copy-l text-xynomix-mako block" data-animate="fade-up">End Date - <?php echo $eventDetails['end_date']; ?></span>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <?php if ($post_type === 'events' && count($categories) > 0) : ?>
                            <span class="xynomix-copy-m mb-2 text-xynomix-raven block" data-animate="fade-up">Category</span>
                            <ul class="list-reset small:mb-4 xxlarge:mb-5 c-checkerboard__categories flex flex-wrap items-center">
                                <?php foreach ($categories as $key => $category) : ?>
                                    <li class="c-checkerboard__categories-item text-xynomix-red-ribbon xynomix-copy px-3 py-1" data-animate="fade-up"><?php echo $category->name; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <?php if ($applications) : ?>
                            <ul class="list-reset small:mb-4 xxlarge:mb-5 c-checkerboard__solutions flex flex-wrap items-center">
                                <?php foreach ($applications as $key => $application) : ?>
                                    <li class="c-checkerboard__solutions-item text-xynomix-red-ribbon xynomix-copy-m" data-animate="fade-up"><?php echo $application['application']; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <p class="xynomix-copy-m text-xynomix-raven small:mb-4 xxlarge:mb-5 xxlarge:mb-8" data-animate="fade-up"><?php echo $details['excerpt']; ?></p>

                        <div data-animate="fade-up">
                            <?php Framework::btnSecondary($btnText, $link); ?>
                        </div>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php endwhile; ?>
    </section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>