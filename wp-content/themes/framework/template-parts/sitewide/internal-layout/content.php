<?php 
    $heading = get_sub_field('heading');
    $feature = get_sub_field('copy_feature');
    $copy = get_sub_field('copy');
?>

<section class="l-container small:mb-11 xxlarge:mb-20">
    <?php if ($heading) : ?>  
        <div class="small-12 small:mb-4 xxlarge:mb-6">
            <div class="small-12 xxlarge-6 max-w-copy-cap-l">
                <h2 class="xynomix-copy-l text-xynomix-mako" data-animate="fade-up"><?php echo $heading; ?></h2>
            </div>
        </div>
    <?php endif; ?>
    
    <div class="row-inner xxlarge:flex clearfix">
        <div class="column small-12 xxlarge-6 small:mb-7 xxlarge:mb-9 max-w-copy-cap-l">
            <?php if ($feature) : ?>  
                <div class="xynomix-content-m text-xynomix-raven" data-animate="fade-up">
                    <?php echo $feature; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="column small-12 xxlarge-6 max-w-copy-cap-l">
            <?php if ($copy) : ?>  
                <div class="xynomix-content-m text-xynomix-raven" data-animate="fade-up">
                    <?php echo $copy; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>