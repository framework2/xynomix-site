<?php 
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
?>

<?php if ($heading || $copy) : ?>
    <section class="l-container small:mb-11 xxlarge:mb-20">
        <div class="max-w-copy-cap-l mx-auto">
            <?php if ($heading) : ?>  
                <h2 class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $heading; ?></h2>
            <?php endif; ?>
            <?php if ($copy) : ?>  
                <div class="xynomix-content-m text-xynomix-raven" data-animate="fade-up">
                    <?php echo $copy; ?>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>