<?php
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
    $button_text = get_sub_field('button_text');
    $button_link = get_sub_field('button_link');
?>

<?php if (!empty($button_text) && !empty($button_link) && (!empty($heading) || !empty($copy)) ) : ?>
<section class="l-container small:mb-11 xxlarge:mb-20">
    <div class="flex flex-wrap rounded-5 c-internal-layout-cta" data-animate="fade-up">
        <div class="small:w-full xxlarge:w-1/2 bg-white small:p-6 xxlarge:py-14">
            <?php if (!empty($heading)) : ?>
                <h2 class="text-xynomix-mako xynomix-heading text-center small:mb-4 xxlarge:mb-6 w-full"><?php echo $heading; ?></h2>
            <?php endif; ?>

            <?php if (!empty($copy)) : ?>
                <p class="text-xynomix-mako xynomix-copy-m text-center mx-auto w-full"><?php echo $copy; ?></p>
            <?php endif; ?>
        </div>
        <div class="small:w-full xxlarge:w-1/2 bg-white small:p-6 xxlarge:p-14 c-internal-layout-cta__button-wrapper flex items-center justify-center">
            <?php Framework::btnSecondaryAlt($button_text, $button_link); ?>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<?php endif; ?>