<?php 
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
?>

<section class="c-internal-layout-feature-text l-container small:mb-11 xxlarge:mb-20">
    <div class="c-internal-layout-feature-text__inner mx-auto">
        <h2 class="xynomix-copy-l text-xynomix-mako text-center small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $heading; ?></h2>
        <p class="xynomix-copy-m text-xynomix-mako text-center" data-animate="fade-up"><?php echo $copy; ?></p>
    </div>
</section>