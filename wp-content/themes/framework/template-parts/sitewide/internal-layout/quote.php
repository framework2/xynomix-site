<?php 
    $quote = get_sub_field('quote');
    $author = get_sub_field('author');
    $company = get_sub_field('company');
?>

<section class="c-internal-layout-quote l-container small:mb-11 xxlarge:mb-20">
    <div class="xxlarge-10 mx-auto">
        <p class="xynomix-copy-l text-xynomix-red-ribbon small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $quote ?></p>
        <p class="xynomix-copy-m text-xynomix-mako" data-animate="fade-up"><?php echo $author; ?></p>
        <p class="xynomix-copy-m text-xynomix-mako" data-animate="fade-up"><?php echo $company; ?></p>
    </div>
</section>