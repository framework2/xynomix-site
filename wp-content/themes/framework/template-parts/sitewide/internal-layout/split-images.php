<?php 
    $imageOne = get_sub_field('image_one');
    $imageTwo = get_sub_field('image_two');
?>

<?php if ($imageOne || $imageTwo) : ?>
    <section class="small:mb-11 xxlarge:mb-20 l-container c-internal-layout-split-images">
        <div class="row-inner large:flex clearfix">
            <?php if ($imageOne) : ?>
                <div class="small-12 large-6 column">
                    <img class="c-internal-layout-split-images__item" src="<?php echo $imageOne; ?>" data-animate="fade-up">
                </div> 
            <?php endif; ?>
            <?php if ($imageTwo) : ?>
                <div class="small-12 large-6 column small:mt-4 large:mt-0">
                    <img class="c-internal-layout-split-images__item" src="<?php echo $imageTwo; ?>" data-animate="fade-up">
                </div> 
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>