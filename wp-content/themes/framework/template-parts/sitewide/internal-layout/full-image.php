<?php 
    $image = get_sub_field('image');
?>

<?php if ($image) : ?>
    <section class="small:mb-11 xxlarge:mb-20">
        <img class="c-internal-layout-full-image w-full" src="<?php echo $image; ?>" data-animate="fade-up">         
    </section>
<?php endif; ?>