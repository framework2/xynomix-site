<?php 
    $heading = get_sub_field('heading');
    $list = get_sub_field('list');
?>

<section class="c-internal-layout-list l-container small:mb-11 xxlarge:mb-20">
    <div class="xxlarge-10 mx-auto">    
        <div class="row-inner">
            <div class="small-12 xxlarge-6 column">
                <?php if ($heading) : ?>
                    <h2 class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $heading; ?></h2>
                <?php endif; ?>
                
                <?php if ($list) : ?>
                    <ul class="list-reset mb-0 c-internal-layout__list">
                        <?php foreach ($list as $key => $item) : ?>
                            <li class="flex items-baseline small:mb-6 xxlarge:mb-8 c-internal-layout__list-item" data-animate="fade-up">
                                <div class="c-internal-layout__list-icon text-xynomix-red-ribbon mr-2 relative">
                                    <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
                                </div>
                                <span class="xynomix-copy-m text-xynomix-raven"><?php echo $item['item']; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>