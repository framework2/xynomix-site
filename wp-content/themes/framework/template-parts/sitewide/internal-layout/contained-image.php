<?php 
    $image = get_sub_field('image');
?>

<?php if ($image) : ?>
    <section class="l-container-l small:mb-11 xxlarge:mb-20">
        <img class="c-internal-layout-contained-image" src="<?php echo $image; ?>" data-animate="fade-up">        
    </section>
<?php endif; ?>