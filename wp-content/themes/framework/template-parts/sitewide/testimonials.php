<?php 
    $args = [
        'post_type' => 'testimonials',
        'post_status' => 'publish',
        'posts_per_page' => '-1',
        'orderby' => 'menu_order',
        'order' => 'ASC',
    ];

    $the_query = new WP_Query($args);
    
?>

<?php if ( $the_query->have_posts() ) : ?>
    <section class="c-testimonials">
        <div class="l-container">
            <div class="row-inner flex justify-between small:mb-4 xxlarge:mb-11">
                <div class="column xxlarge-6 c-testimonials-slider--images small:hidden xxlarge:block" data-animate="fade-up">   
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <?php 
                        $image = get_field('details')['image'];
                    
                    ?>
                        <div class="c-testimonials__image bg-center bg-cover bg-no-repeat pt-100% w-full rounded-large" style="background-image:url('<?php echo $image; ?>');" ></div>
                    <?php endwhile; ?>
                </div>
                <div class="column xxlarge-5 p-6">
                    <div class="mb-9">
                        <div class="w-full">
                            <h2 class="text-xynomix-mako xynomix-heading small:mb-4 xxlarge:mb-5" data-animate="fade-up">Testimonials</h2>
                            <div class="c-testimonials__slider">
                                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                    
                                    <?php 
                                        $details = get_field('details');
                                    ?>
                                    <div class="w-full c-testimonials__slider-item">
                                        <p class="text-xynomix-mako xynomix-copy mb-6" data-animate="fade-up"><?php echo $details['quote']; ?></p>
                                        <h3 class="xynomix-copy-l text-xynomix-red-ribbon mb-2" data-animate="fade-up"><?php echo $details['client_name']; ?></h3>
                                        <h3 class="xynomix-copy-l text-xynomix-red-ribbon" data-animate="fade-up"><?php echo $details['company']; ?></h3>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                    </div>

                    <div class="flex" data-animate="fade-up">
                        <?php Framework::btnCircle(null, null, 'mr-5 c-testimonials__prev', 'span'); ?>
                        <?php Framework::btnCircle(null, null, 'c-testimonials__next', 'span'); ?>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="l-container">
            <div class="xxlarge-6 flex justify-center row-inner">
                <div class="c-testimonials__slider-icon"></div>
            </div>
        </div>
        
        <div class="bg-black py-6 overflow-hidden">
            <div class="l-container">
                <div class="xxlarge:row-inner small-12 xxlarge-6 c-testimonials__slider--logos"> 
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <?php 
                            $logo = get_the_post_thumbnail_url();
                        ?>
                        <div class="column small-12 flex items-center justify-center min-h-full" data-animate="fade-in">
                            <img class="c-testimonials__logo" src="<?php echo $logo; ?>">
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </section>
<?php wp_reset_postdata(); ?>
<?php endif; ?>