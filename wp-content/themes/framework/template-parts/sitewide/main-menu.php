<?php 

$theme_locations = get_nav_menu_locations();
$menu_object = get_term($theme_locations['primary'], 'nav_menu');

if (is_wp_error($menu_object) === false): ?>

<?php $menu = new TheFramework\Menu\Menu( $menu_object->name ); ?>

    <?php if (count($menu->items) > 0): ?>
	

        <?php 
			$name = 'c-main-menu';
        ?>

		
	
		<ul class="<?php echo $name; ?> list-reset mb-0 xxlarge:mr-0 small:flex small:flex-col small:items-end xxlarge:items-center xxlarge:flex-row z-10">
			<?php foreach ($menu->items as $key => $item): ?>
			
				<?php 
					$submenu = $menu->get_submenu($item);
					$page_id = get_queried_object_id();
					$itemClasses = [];
					$classes = implode(' ', $item->classes);
					$textClasses = [];

					if ($submenu) {
						array_push($textClasses, $name.'__item--has-submenu');
						
						foreach ($submenu as $key => $subItem) {
							if ( $page_id == $subItem->object_id){
								array_push($itemClasses, $name.'__item--childActive');
							} 
						}	
					}

					if ($page_id == $item->object_id) {
						array_push($itemClasses, $name.'__item--active');
					} 

					$itemClasses = implode(' ', $itemClasses);
					$textClasses = implode(' ', $textClasses);
				?>

				<li class="<?php echo $name; ?>__item inline-block relative <?php echo $itemClasses; ?>">
					<a class="<?php echo $name; ?>__link text-black <?php echo $classes; ?> small:py-0 small:mb-6 xxlarge:mb-0 xxlarge:py-6 inline-flex items-center" href="<?php echo $item->url ?>"><span class="<?php echo $textClasses; ?> <?php echo $name; ?>__text"><?php echo $item->title; ?></span>
						<?php if ($submenu) : ?>
							<span class="small:hidden xxlarge:inline-block relative ml-1 <?php echo $name; ?>__link-icon">
								<?php Framework::svgIcon('dropdown-arrow', '0 0 8 5'); ?>
							</span>
						<?php endif; ?>
					</a>
					
					
					<?php if ($submenu) : ?>
						<div class="<?php echo $name?>__dropdown small:hidden xxlarge:flex bg-white absolute">
							<div class="<?php echo $name?>__dropdown-column w-2/3">
								<div class="bg-black flex items-center justify-center p-2">
									<span class="<?php echo $name?>__dropdown-heading text-white"><?php echo $item->title; ?></span>
								</div>

								<ul class="<?php echo $name; ?>__submenu list-reset mb-0 flex flex-wrap">
									<?php foreach ($submenu as $key => $subItem) : ?>

										<?php
											$title = $subItem->title;
											$url = $subItem->url;
											$subActiveClass = "";
											$details = get_field('Details', $subItem->object_id);

											

											if ( $page_id == $subItem->object_id) {
												$subActiveClass = $name.'__submenu-item--active';
											} 
										?>

										<li class="<?php echo $name; ?>__submenu-item w-1/4 <?php echo $subActiveClass; ?>">
											<a href="<?php echo $url; ?>" class="flex flex-col items-center py-5 px-3 h-full">
												<div class="bg-center bg-contain bg-no-repeat mb-3 <?php echo $name; ?>__submenu-icon" style="background-image:url('<?php echo $details['icon']; ?>');">
												</div>
												<span class="<?php echo $name; ?>__submenu-link text-xynomix-mako text-center mt-auto mb-auto w-full"><?php echo $title; ?></span>
											</a>
										</li>
									<?php endforeach; ?>
								</ul>								
							</div>
							<div class="<?php echo $name?>__dropdown-column w-1/3">
								<div class="bg-black flex items-center justify-center p-2">
									<span class="<?php echo $name?>__dropdown-heading text-white">Information</span>
								</div>
								
								<div class="px-8 py-10 <?php echo $name?>__dropdown-slider w-full">
									
									<?php 
										$hero = get_field('hero', $item->object_id);
										//debug($hero, true);
									?>

									<div class="w-full c-main-menu__dropdown-slider-item">
										<h3 class="mb-4 xynomix-heading-xs text-xynomix-mako"><?php echo $hero['heading']; ?></h3>

										<p class="xynomix-copy text-xynomix-raven"><?php echo $hero['subheading']; ?></p>
									</div>
									<?php foreach ($submenu as $key => $subItem) : ?>

										<?php 
											$title = $subItem->title;
											$excerpt = get_field('Details', $subItem->object_id)['excerpt'];
										?>
										<div class="w-full c-main-menu__dropdown-slider-item">
											<h3 class="mb-4 xynomix-heading-xs text-xynomix-mako"><?php echo $title; ?></h3>

											<p class="xynomix-copy text-xynomix-raven"><?php echo $excerpt; ?></p>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</li>

			<?php endforeach; ?>
		</ul>

	<?php endif; ?>

<?php endif; ?>