<?php
    $image = get_the_post_thumbnail_url();
    $title = get_the_title();
    $details = get_field('project_details');
    $link = get_the_permalink();  
    $post_type = get_post_type();
    $btnLink = home_url();
    $eventCategories = wp_get_post_terms(get_the_ID(), 'event-categories');
    $newsCategories = wp_get_post_terms(get_the_ID(), 'news-categories');
    $eventDetails = get_field('event_details');
    $date = get_the_date('d/m/Y');


    switch ( $post_type ) {
        case 'job-vacancies':                 
            $archive_args = array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-about-template.php',
            );
            break;  
        case 'case-studies':                 
            $archive_args = array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-case-studies-archive-template.php',
            );
            break;  
        case 'news':                 
            $archive_args = array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-resources-template.php',
            );
            break;  
        case 'events':                 
            $archive_args = array(
                'post_type' => 'page',
                'meta_key' => '_wp_page_template',
                'meta_value' => 'page-events-template.php',
            );
            break;              
    }

    if (isset($archive_args)) {
        $btnLink = get_permalink(get_pages($archive_args)[0]->ID);  
    }
?>

<section class="overflow-hidden w-full small:mb-10 xxlarge:mb-14">
    <div class="c-hero c-hero--internal-alt bg-center bg-cover bg-no-repeat flex justify-center items-center" style="background-image:url('<?php echo $image; ?>');">
        <div class="relative w-full">
            <?php get_template_part('template-parts/sitewide/hero-social'); ?>
            <div class="c-hero__content small:l-container super:l-container-l">
                <div class="small-12 xxlarge-10 super-12 mx-auto">
                    <?php Framework::btnBack('Back', $btnLink, 'mb-11'); ?>
                </div>
                <div class="super:l-container">
                    <div class="small-10 mx-auto">
                        <h1 class="xynomix-heading text-white mb-3"><?php echo $title; ?></h1>
                        <?php if ($details['company_name']) ?><h2 class="text-white xynomix-copy-m mb-8"><?php echo $details['company_name']; ?></h2>                             
                                                
                        <?php if ($post_type === 'events' && count($eventCategories) > 0) : ?>
                            <span class="xynomix-copy-m mb-2 text-white block">Category</span>
                            <ul class="list-reset small:mb-2 xxlarge:mb-3 c-hero--internal-alt__categories flex flex-wrap items-center">
                                <?php foreach ($eventCategories as $key => $category) : ?>
                                    <li class="c-hero--internal-alt__categories-item text-white xynomix-copy px-3 py-1 mb-2"><?php echo $category->name; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>


                        <?php if ($post_type === 'news' && count($newsCategories) > 0) : ?>
                            <span class="xynomix-copy-m mb-2 text-white block">Category</span>
                            <ul class="list-reset small:mb-2 xxlarge:mb-3 c-hero--internal-alt__categories flex flex-wrap items-center">
                                <?php foreach ($newsCategories as $key => $category) : ?>
                                    <li class="c-hero--internal-alt__categories-item text-white xynomix-copy px-3 py-1 mb-2"><?php echo $category->name; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>        

                        <?php if ($post_type === 'news') : ?>
                            <p class="xynomix-copy-m text-white mb-2">Published on: <?php echo $date; ?></p>
                        <?php endif; ?>                                        

                        <span class="block text-white xynomix-copy mb-1">Share</span>
                        <div class="flex items-center"> 

                            <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>" target="_blank" class="l-footer__social-icon l-footer__social-linkedin text-white relative">
                                <?php Framework::svgIcon('linkedin', '0 0 17 17'); ?>
                            </a>

                            <!-- <a href="https://plus.google.com/share?url=<?php // echo $link; ?>" target="_blank" class="l-footer__social-icon l-footer__social-googleplus text-white relative">
                            <?php // Framework::svgIcon('googleplus', '0 0 25 16'); ?></a> -->
                            
                            <a href="https://twitter.com/home?status=<?php echo $link; ?>" target="_blank" class="l-footer__social-icon l-footer__social-twitter text-white relative">
                            <?php Framework::svgIcon('twitter', '0 0 19 15'); ?></a>

                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" target="_blank" class="l-footer__social-icon l-footer__social-facebook text-white relative">
                                <?php Framework::svgIcon('facebook', '0 0 18 16'); ?>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>