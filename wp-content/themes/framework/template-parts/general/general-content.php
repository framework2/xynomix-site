<?php 
    $content = get_field('content');
    
?>

<section class="l-container">
    <?php foreach ($content['content'] as $key => $block) : ?>
        <div class="small:mb-10 xxlarge:mb-20">
            <h2 class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $block['heading']; ?></h2>
            <div class="xynomix-content text-xynomix-raven" data-animate="fade-up"><?php echo $block['copy']; ?></div>
        </div>
    <?php endforeach; ?>
</section>