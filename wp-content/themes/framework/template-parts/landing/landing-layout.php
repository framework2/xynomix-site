<?php if (have_rows('layout')): ?>
    <?php while (have_rows('layout') ) : the_row(); ?>
        <?php switch ( get_row_layout() ) {
            case 'centered_content':                 
                get_template_part('template-parts/landing/centered-content', null);
                break;                 
            case 'full_image':                 
                get_template_part('template-parts/sitewide/internal-layout/full-image', null);
                break;              
            case 'list':                 
                get_template_part('template-parts/landing/list', null);
                break;            
            case 'cta':                 
                get_template_part('template-parts/landing/cta', null);
                break;    
            case 'slider':                 
                get_template_part('template-parts/landing/slider', null);
                break;   
            case 'content_cta':                 
                get_template_part('template-parts/landing/content-cta', null);
                break;   
            case 'logo_block':                 
                get_template_part('template-parts/landing/logos', null);
                break;                                                                   
        } ?>
    <?php endwhile; ?>
<?php endif; ?>