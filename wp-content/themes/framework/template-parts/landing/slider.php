<?php 
  $slides = get_sub_field('slides');
?>

<?php if (!empty($slides)) : ?>
<div class="overflow-hidden small:mb-11 xxlarge:mb-20 c-landing-slider-wrapper">
  <section class="l-container">
    <div class="relative">
      <div class="row-inner">
        <div class="c-landing-slider w-full large-10 mx-auto">
          <?php foreach ($slides as $key => $slide) : ?>
            <div class="column w-full c-landing-slider__item">
              <div class="small:px-6 xlarge:px-14 small:py-4 xlarge:py-12 rounded-large border border-xynomix-alto min-h-full c-landing-slider__item">
                <?php if ($slide['heading']) : ?>  
                  <div class="flex items-center w-full small:mb-4 xxlarge:mb-6">
                    <?php if (!empty($slide['icon'])) : ?>
                      <img class="mr-6" style="max-width:80px;" src="<?php echo $slide['icon']; ?>" data-animate="fade-up"/>
                    <?php endif; ?>
                    <h2 class="xynomix-heading-s text-xynomix-mako " data-animate="fade-up"><?php echo $slide['heading']; ?></h2>
                  </div>
                <?php endif; ?>
                <?php if (!empty($slide['copy'])) : ?>
                  <div class="xynomix-content-m text-xynomix-raven" data-animate="fade-up"><?php echo $slide['copy']; ?></div>
                <?php endif; ?>
              </div>
            </div>
          <?php endforeach; ?>
        </div>   
      </div>

      <?php if (count($slides) > 1) : ?>    
        <div class="row-inner flex justify-center small:mt-4 xxlarge:mt-6">
          <div>
            <div class="column">
                <?php Framework::btnCircle(null, null, 'c-landing-slider__prev btn--circle-left', 'span') ?>
            </div>
          </div>      
          <div>
            <div class="column"> 
                <?php Framework::btnCircle(null, null, 'c-landing-slider__next', 'span') ?>
            </div>
          </div>     
        </div>  
      <?php endif; ?>
    </div>
  </section>
</div>
<?php endif; ?>