<?php
    $cta = get_sub_field('cta');
    $content = get_sub_field('content');
?>

<?php if (!empty($content)) : ?>
<section class="l-container small:mb-11 xxlarge:mb-20">
    <div class="row-inner xlarge:flex items-center justify-between">
        <div class="small:w-full large:w-1/2">
            <div class="column">
                <div class="xynomix-content-m text-xynomix-raven" data-animate="fade-up">
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <div class="small:w-full large:w-5/12 ml-auto">
            <div class="column">
                <div class="rounded-large bg-xynomix-red-ribbon" data-animate="fade-up">
                    <div class="small:w-full small:p-6 xxlarge:py-8">
                        <?php if (!empty($cta['heading'])) : ?>
                            <h2 class="text-white xynomix-heading-s text-center small:mb-4 xxlarge:mb-4 w-full"><?php echo $cta['heading']; ?></h2>
                        <?php endif; ?>

                        <?php if (!empty($cta['copy'])) : ?>
                            <p class="text-white xynomix-copy-m text-center mx-auto w-full small:mb-4 xlarge:mb-8 max-w-copy-cap-l"><?php echo $cta['copy']; ?></p>
                        <?php endif; ?>

                        <div class="flex justify-center">
                        <?php Framework::btnPrimaryAlt3($cta['button_text'], $cta['button_link']); ?>            
                        </div>
                    </div>
                </div> 
            </div>       
        </div>
    <div>
    <div class="clearfix"></div>
</section>
<?php endif; ?>