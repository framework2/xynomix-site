<?php
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
    $button_text = get_sub_field('button_text');
    $button_link = get_sub_field('button_link');
?>

<?php if (!empty($button_text) && !empty($button_link) && (!empty($heading) || !empty($copy)) ) : ?>
<section class="l-container small:mb-11 xxlarge:mb-20">
    <div class="rounded-large bg-xynomix-red-ribbon large:w-4/5 xlarge:w-2/3 mx-auto" data-animate="fade-up">
        <div class="small:w-full small:p-6 xxlarge:py-8">
            <?php if (!empty($heading)) : ?>
                <h2 class="text-white xynomix-heading-s text-center small:mb-4 xxlarge:mb-4 w-full"><?php echo $heading; ?></h2>
            <?php endif; ?>

            <?php if (!empty($copy)) : ?>
                <p class="text-white xynomix-copy-m text-center mx-auto w-full small:mb-4 xlarge:mb-8 max-w-copy-cap-l"><?php echo $copy; ?></p>
            <?php endif; ?>

            <div class="flex justify-center">
              <?php Framework::btnPrimaryAlt3($button_text, $button_link); ?>            
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<?php endif; ?>