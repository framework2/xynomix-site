<?php 
  $list = get_sub_field('list');
?>

<?php if (!empty($list)) : ?>
  <section class="l-container small:mb-11 xxlarge:mb-20">
    <div class="mx-auto small:w-full xlarge:w-5/6">
      <ul class="row-inner large:flex flex-wrap list-reset small:-mb-6 xxlarge:-mb-8">
          <?php foreach ($list as $key => $item) : ?>
            <li class="small:mb-6 xxlarge:mb-8 small:w-full large:w-1/2 c-internal-layout__list-item" data-animate="fade-up">
                <div class="column flex items-baseline column">
                  <div class="c-internal-layout__list-icon text-xynomix-red-ribbon mr-2 relative">
                      <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
                  </div>
                  <div class="xynomix-content-m text-xynomix-raven"><?php echo $item['content']; ?></div>
                </div>
            </li>         
          <?php endforeach; ?>
      </ul>
    </div>
    <div class="clearfix"></div>
  </section>
<?php endif; ?>