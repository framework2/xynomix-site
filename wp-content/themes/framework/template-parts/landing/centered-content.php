<?php 
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
?>

<?php if ($heading || $copy) : ?>
<div class="small:mb-11 xxlarge:mb-20 inline-block w-full">
  <section class="l-container">
      <div class="max-w-copy-cap-l mx-auto">
          <?php if ($heading) : ?>  
              <h2 class="xynomix-heading-s text-center text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $heading; ?></h2>
          <?php endif; ?>
          <?php if ($copy) : ?>  
              <div class="xynomix-content-m text-center text-xynomix-raven" data-animate="fade-up">
                  <?php echo $copy; ?>
              </div>
          <?php endif; ?>
      </div>
  </section>
</div>
<?php endif; ?>