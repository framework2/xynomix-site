<?php 
  $logos = get_sub_field('logos');

?>

<?php if (!empty($logos)) : ?>
  <section class="l-container small:mb-11 xxlarge:mb-20">
    <div class="row-inner flex flex-wrap clearfix justify-center -mb-6 items-center">
      <?php foreach ($logos as $key => $logo) : ?>
        <div class="column mb-6 inline-block" style="max-width: 195px;" data-animate="fade-up">
          <div class="w-full relative" style="padding-top: 25.77%;">
            <figure class="absolute pin w-full h-full flex items-center">
              <img class="w-full" src="<?php echo $logo['url']; ?>"/>
            </figure>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
  </section>
<?php endif; ?>