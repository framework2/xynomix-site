<?php 
$categories = get_terms([
    'taxonomy' => 'event-categories',
    'hide_empty' => false,
]);

$dates = get_posts_years_array(['post_type' => 'events']);
?>

<section class="w-full bg-xynomix-red-ribbon c-events-filter">
    <div class="l-container">
        <div class="clearfix">
        <div class="small-12 xxlarge-7">
            <div class="c-events-filter__item small-12">
                <h3 class="text-white inline-flex items-center c-events-filter__btn c-events-filter__heading cursor-pointer small:py-2 xxlarge:py-4">
                    Filter Events
                    <span class="inline-block relative ml-1 c-events-filter__icon z-0">
                        <?php Framework::svgIcon('dropdown-arrow', '0 0 8 5'); ?>
                    </span>        
                </h3>
            </div>
            <div class="c-events-filter__dropdown overflow-hidden">
                <div class="xxlarge:flex flex-wrap row-inner small:py-4 xxlarge:py-4">
                    <div class="c-events-filter__item column small-12 large-4 small:mb-4 large:mb-0">
                        <h3 class="text-white inline-flex items-center c-events-filter__heading small:mb-2 xxlarge:mb-4">Categories</h3>
                        
                        <div class="c-events-filter__list">
                            <?php foreach ($categories as $key => $category) : ?>
                                <div class="c-events-filter__category" data-category="<?php echo $category->slug; ?>">
                                    <?php Framework::btnArrow($category->name, null, 'mb-1', 'span'); ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>


                    <div class="c-events-filter__item column small-12 large-4 small:mb-4 large:mb-0">
                        <h3 class="text-white inline-flex items-center c-events-filter__heading small:mb-2 xxlarge:mb-4">Dates</h3>

                        <div class="c-events-filter__list">
                            <?php foreach ($dates as $key => $date) : ?>
                                <div class="c-events-filter__date" data-year="<?php echo $date['year']; ?>" data-month="<?php echo $date['month']; ?>">
                                    <?php Framework::btnArrow($date['month'] . ' ' . $date['year'], null, 'mb-1', 'span'); ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="c-events-filter__item column small-12 large-4">
                        <?php Framework::btnPrimaryAlt2('Apply filters', null, 'c-events-filter__filterBtn', 'span'); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>      
    </div>
</section>