<?php if (have_rows('layout')): ?>
    <?php while (have_rows('layout') ) : the_row(); ?>
        <?php switch ( get_row_layout() ) {
            case 'content':                 
                get_template_part('template-parts/jobs/jobs-layout/content', null);
                break;  
            case 'list_split':                 
                get_template_part('template-parts/jobs/jobs-layout/list-split', null);
                break;  
        } ?>
    <?php endwhile; ?>
<?php endif; ?>