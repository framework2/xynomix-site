<?php 
    $heading = get_sub_field('heading');
    $copy = get_sub_field('copy');
?>

<section class="c-jobs-content small:mb-10 xxlarge:mb-20 l-container">
    <h2 class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $heading; ?></h2>
    <div class="xynomix-content text-xynomix-raven" data-animate="fade-up"><?php echo $copy; ?></div>
</section>