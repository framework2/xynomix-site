<?php 
    $heading = get_sub_field('heading');
    $listOne = get_sub_field('list_one');
    $listTwo = get_sub_field('list_two');
?>

<section class="c-jobs-list-split l-container small:mb-10 xxlarge:mb-20">
    <div class="small-12 mx-auto">    
        <div class="row-inner large:flex">
            <div class="small-12 large-6 column small:mb-10 large:mb-0">
                <?php if ($listOne['heading']) : ?>
                    <h2 class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $listOne['heading']; ?></h2>
                <?php endif; ?>
                
                <?php if ($listOne['list']) : ?>
                    <ul class="list-reset mb-0 c-jobs-list-split__list">
                        <?php foreach ($listOne['list'] as $key => $item) : ?>
                            <li class="flex items-baseline small:mb-6 xxlarge:mb-8 c-jobs-list-split__list-item" data-animate="fade-up">
                                <div class="c-jobs-list-split__list-icon text-xynomix-red-ribbon mr-2 relative">
                                    <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
                                </div>
                                <span class="xynomix-copy-m text-xynomix-raven"><?php echo $item['item']; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="small-12 large-6 column">
                <?php if ($listTwo['heading']) : ?>
                    <h2 class="xynomix-copy-l text-xynomix-mako small:mb-4 xxlarge:mb-6" data-animate="fade-up"><?php echo $listTwo['heading']; ?></h2>
                <?php endif; ?>
                
                <?php if ($listTwo['list']) : ?>
                    <ul class="list-reset mb-0 c-jobs-list-split__list">
                        <?php foreach ($listTwo['list'] as $key => $item) : ?>
                            <li class="flex items-baseline small:mb-6 xxlarge:mb-8 c-jobs-list-split__list-item" data-animate="fade-up">
                                <div class="c-jobs-list-split__list-icon text-xynomix-red-ribbon mr-2 relative">
                                    <?php Framework::svgIcon('arrow', '0 0 13 11'); ?>
                                </div>
                                <span class="xynomix-copy-m text-xynomix-raven"><?php echo $item['item']; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</section>