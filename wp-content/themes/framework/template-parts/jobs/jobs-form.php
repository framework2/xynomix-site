<?php 
    $details =  get_field('company_details', 'option');
?>

<div class="overflow-hidden">
    <section class="c-jobs-form">
        <div class="l-container">
            <div class="xxlarge-10 mx-auto flex flex-col items-center">
                <h2 class="xynomix-heading text-xynomix-mako mb-9 text-center" data-animate="fade-up">Apply Now</h2>
                <div class="large-6 small-12">
                    <div class="c-jobs-form__form" data-animate="fade-up">
                        <?php echo do_shortcode('[contact-form-7 id="442" title="Contact Job"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>