<?php 
    /* Template Name: Services Template */
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<?php if (1 === 0) : ?>
<div>
    <?php 
        $post_type = 'service';
        include_once( locate_template('template-parts/sitewide/checkerboard.php')); 
    ?>
</div>
<?php endif; ?>

<?php 
$details = get_field('intro');
include_once( locate_template('template-parts/sitewide/services-cta.php')); 
?>

<div class="small:mt-6">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>
<?php get_footer(); ?>