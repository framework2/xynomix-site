<?php 
    /* Template Name: Partners Archive Template */
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<div>
    <?php 
        $post_type = 'partners';
        include_once( locate_template('template-parts/sitewide/checkerboard.php')); 
    ?>
</div>

<div class="small:mt-6 large:-mt-4">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>

<?php get_footer(); ?>