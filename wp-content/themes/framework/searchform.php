<div class="search-box clearfix">
	<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
		<input class="search-input"  type="search" name="s" placeholder="Search">
		<input class="search-submit" type="submit" value="<?php _e( 'Search', 'theframework' ); ?>">
	</form>
</div>