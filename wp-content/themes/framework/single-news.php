
<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', 'alt'); ?>

<?php get_template_part('template-parts/sitewide/internal-layout', null); ?>

<?php 
    $archive_args = array(
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-resources-template.php',
    );

    $archive_url = get_permalink(get_pages($archive_args)[0]->ID);    
?>

<div class="flex items-center justify-center small:mb-10 xxlarge:mb-20" data-animate="fade-in">
    <?php Framework::btnSecondary('Back to all articles', $archive_url); ?>
</div>

<?php get_template_part('template-parts/news/news', 'pagination'); ?>

<div class="small:mt-6 large:-mt-6"> 
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>

<?php get_footer(); ?>