<?php 
  /* Template Name: Landing Template */
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<?php get_template_part('template-parts/landing/landing-layout', null); ?>

<?php get_footer(); ?>