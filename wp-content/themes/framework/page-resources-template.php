<?php 
    /* Template Name: Resources Archive Template */
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<?php get_template_part('template-parts/resources/resources', 'grid'); ?>

<div class="small:mb-10 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/events-cta', null); ?>
</div>

<?php get_template_part('template-parts/resources/resources', 'faqs'); ?>

<div class="small:mt-6 large:-mt-4">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>
<?php get_footer(); ?>