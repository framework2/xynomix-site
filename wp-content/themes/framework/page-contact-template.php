<?php 
    /* Template Name: Contact Template */
?>

<?php get_header(); ?>

    <?php get_template_part('template-parts/sitewide/hero-internal', null); ?>
    <?php get_template_part('template-parts/contact/contact', 'buckets'); ?>
    <?php get_template_part('template-parts/contact/contact', 'map'); ?>

<?php get_footer(); ?>