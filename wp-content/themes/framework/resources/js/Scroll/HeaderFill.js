class HeaderFill {

	constructor() {

		this.header = document.querySelector('.l-header');
		this.body = document.body;

		if (!_.isNull(this.header)) {
			this.resize();
		}

		const headroom = new Headroom(this.header, {
			offset: 50,
		});
		headroom.init();

	}

	resize() {

		if (this.body.classList.contains('page-template-page-events-template')) {

			///this.header.classList.add('l-header--fill');
			const headerHeight = parseFloat(window.getComputedStyle(this.header, null).getPropertyValue('height').slice(0, -2));
			this.body.style.paddingTop = `${headerHeight}px`;
		}

		// } else {
		// 	window.onscroll = () => {
		// 		const headerHeight = parseFloat(window.getComputedStyle(this.header, null).getPropertyValue('height').slice(0, -2));

		// 		if (window.pageYOffset > (headerHeight)) {
		// 			this.header.classList.add('l-header--fill');
		// 		} else {
		// 			this.header.classList.remove('l-header--fill');
		// 		}
		// 	};
		// }
	}
}

module.exports = HeaderFill;