/**
 *
 * checkBrowserSize() method
 *
 * check the current browsers width
 * the true width in case you have the
 * dev toolbar open in right side
 *
 * @return actualSize the actual width
 *
 */
module.exports = {

    /**
     * breakpoint
     *
     * The breakpoint object to get the current brkpt
     * from the css grid system
     *
     * body:before {} - either in app.scss or base-css.scss or similar
     *
     * This will be a bit of text added to page but then hidden
     * we then pick it up in breakpoint{}
     *
     * This way our js syncs up with our CSS breakpoints
     *
     * Just need to call/check breakpoint.grp or breakpoint.grpInt
     *
     * @since   1.0
     * @version 1.0
     */

    value: '',
    /*
     * init is just nicer way to start it off
     *
     */
    init(){
        this.refreshValue();
    },

    /*
     * the guts of this file
     */
    refreshValue() {

        //console.error(window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content'));

        var grp = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/"/g, '');
        var int = window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/[a-zA-Z\-'"]+/g, '');

        this.value = grp;
        this.grpInt = parseInt(int);

        Breakpoint.value = grp;
        Breakpoint.grpInt = parseInt(int);
        return this.grpInt;
    }
};

//exports.breakpoint = breakpoint;