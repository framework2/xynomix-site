class LandingSliders {
  constructor() {
    this.carousels = document.querySelectorAll(".c-landing-slider-wrapper");

    if (_.isNull(this.carousels) === false) {
      this.initCarousels();
    }
  }

  initCarousels() {
    Array.prototype.forEach.call(this.carousels, el => {
      const prev = document.querySelector(".c-landing-slider__prev");
      const next = document.querySelector(".c-landing-slider__next");
      const slider = document.querySelector(".c-landing-slider");

      const flkty = new Flickity(slider, {
        cellAlign: "center",
        contain: true,
        prevNextButtons: false,
        pageDots: true,
        freeScroll: false,
        adaptiveHeight: true
      });

      flkty.on("dragStart", (event, pointer) => {
        document.ontouchmove = e => {
          e.preventDefault();
        };
      });

      flkty.on("dragEnd", (event, pointer) => {
        document.ontouchmove = e => {
          return true;
        };
      });

      if (
        _.isNull(prev) === false &&
        _.isNull(next) === false &&
        _.isNull(flkty) === false
      ) {
        this.initControls(flkty, prev, next);
      }
    });
  }

  initControls(flkty, prev, next) {
    next.addEventListener(
      "click",
      e => {
        e.preventDefault();
        flkty.next(true, false);
      },
      false
    );

    prev.addEventListener(
      "click",
      e => {
        e.preventDefault();
        flkty.previous(true, false);
      },
      false
    );
  }
}

module.exports = LandingSliders;
