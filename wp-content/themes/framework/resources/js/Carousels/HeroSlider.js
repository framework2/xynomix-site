class HeroSlider {

    constructor() {

        this.carousel = document.querySelector('.c-hero__slider');

        if (_.isNull(this.carousel) === false) {
            this.initCarousel();
        }
    }

    initCarousel() {
        this.flkty = new Flickity(this.carousel, {
            draggable: false,
            cellAlign: 'center',
            contain: true,
            prevNextButtons: false,
            wrapAround: true,
            pageDots: true,
            autoPlay: 5000,
            pauseAutoPlayOnHover: false,
            //watchCSS: true,
            freeScroll: false,
            adaptiveHeight: false,
        });

        this.flkty.on('dragStart', (event, pointer) => {
            document.ontouchmove = (e) => {
                e.preventDefault();
            };
        });

        this.flkty.on('dragEnd', (event, pointer) => {
            document.ontouchmove = (e) => {
                return true;
            };
        });
    }
}

module.exports = HeroSlider;