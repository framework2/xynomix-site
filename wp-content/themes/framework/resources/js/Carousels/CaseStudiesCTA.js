class CaseStudiesCTA {

    constructor() {

        this.carousel = document.querySelector('.c-case-studies-cta__slider');
        this.prev = document.querySelector('.c-case-studies-cta__prev');
        this.next = document.querySelector('.c-case-studies-cta__next');

        if (_.isNull(this.carousel) === false) {
            this.initCarousel();
        }
    }

    initCarousel() {
        this.flkty = new Flickity(this.carousel, {
            cellAlign: 'center',
            contain: true,
            prevNextButtons: false,
            pageDots: true,
            freeScroll: false,
            adaptiveHeight: false,  
        });


        if (_.isNull(this.prev) === false && _.isNull(this.next) === false) {
            this.initControls();
        }
    }

    initControls() {
        this.next.addEventListener('click', (e) => {
            this.flkty.next(true, false);
        });

        this.prev.addEventListener('click', (e) => {
            this.flkty.previous(true, false);
        });
    }
}

module.exports = CaseStudiesCTA;