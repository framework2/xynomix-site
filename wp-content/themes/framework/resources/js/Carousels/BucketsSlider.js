class BucketsSlider {

    constructor() {

        this.carousel = document.querySelector('.c-home-buckets');

        if (_.isNull(this.carousel) === false) {
            this.initCarousel();
        }
    }

    initCarousel() {
        this.flkty = new Flickity(this.carousel, {
            cellAlign: 'left',
            contain: true,
            prevNextButtons: false,
            pageDots: true,
            watchCSS: true,
            freeScroll: true,
            adaptiveHeight: false,
        });

        this.flkty.on('dragStart', (event, pointer) => {
            document.ontouchmove = (e) => {
                e.preventDefault();
            };
        });

        this.flkty.on('dragEnd', (event, pointer) => {
            document.ontouchmove = (e) => {
                return true;
            };
        });
    }
}

module.exports = BucketsSlider;