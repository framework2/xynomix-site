class ResourcesCarousel {

    constructor() {

        this.carousel = document.querySelector('.c-resources-grid__slider');
        this.paginationBtns = document.querySelectorAll('.c-resources-grid__pagination-btn-wrapper');

        if (_.isNull(this.carousel) === false) {
            this.initCarousel();
        }
    }

    initCarousel() {
        this.flkty = new Flickity(this.carousel, {
            cellAlign: 'left',
            draggable: false,
            contain: true,
            prevNextButtons: false,
            pageDots: false,
            freeScroll: false,
            adaptiveHeight: true,
        });


        if (!_.isNull(this.paginationBtns)) {
            this.initControls();
        }
    }

    initControls() {
        Array.prototype.forEach.call(this.paginationBtns, (el, i) => {

            const slideId = el.getAttribute('data-slide');
            const btn = el.querySelector('.btn');                           
            el.addEventListener('click', () => {

                Array.prototype.forEach.call(this.paginationBtns, (el2, i2) => {
                    const btnInner = el2.querySelector('.btn');
                    btnInner.classList.remove('active'); 
                });
                
                this.flkty.select(slideId);
                btn.classList.add('active'); 
            });
        });
    }
}

module.exports = ResourcesCarousel;