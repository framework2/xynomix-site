class TestimonialsSlider {

    constructor() {

        //this.imgsToLoad = document.querySelectorAll('.c-testimonials__logo');
        // this.imgLength = this.imgsToLoad.length;
        //this.loaded = 0;
        this.quoteSlider = document.querySelector('.c-testimonials__slider');
        this.logoSlider = document.querySelector('.c-testimonials__slider--logos');
        this.imageSlider = document.querySelector('.c-testimonials-slider--images');
        this.prev = document.querySelector('.c-testimonials__prev');
        this.next = document.querySelector('.c-testimonials__next');

        if (!_.isNull(this.quoteSlider) && !_.isNull(this.logoSlider)) {
            this.initCarousels();
        }
    }

    // checkCanPreload() {
    //     if (this.loaded >= this.imgLength) {
    //         this.initCarousels();
    //     }
    // }

    // preloadImgs() {
    //     Array.prototype.forEach.call(this.imgsToLoad, (el, i) => {
    //         this.loadImage(el);
    //     });
    // }

    // loadImage(el) {
    //     const imgUrl = el.getAttribute('data-img-src');
    //     const that = this;
    //     const img = new Image();
    //     img.onload = () => {
    //         el.style.backgroundImage = `url('${imgUrl}')`;
    //         el.style.height = `${this.height}px`;
    //         el.style.width = `${this.width}px`;
    //         that.loaded += 1;
    //         that.checkCanPreload();
    //     };

    //     img.src = imgUrl;
    // }

    initCarousels() {


        imagesLoaded(this.quoteSlider, (instance) => {
            this.quoteFlkty = new Flickity(this.quoteSlider, {
                draggable: false,
                cellAlign: 'center',
                contain: true,
                prevNextButtons: false,
                pageDots: false,
                freeScroll: false,
                adaptiveHeight: false,
            });
        });

        imagesLoaded(this.logoSlider, (instance) => {
            this.logoFlkty = new Flickity(this.logoSlider, {
                draggable: false,
                cellAlign: 'left',
                contain: true,
                prevNextButtons: false,
                pageDots: false,
                freeScroll: false,
                adaptiveHeight: false,
            });
        });


        imagesLoaded(this.imageSlider, (instance) => {
            this.imageFlkty = new Flickity(this.imageSlider, {
                draggable: false,
                cellAlign: 'left',
                contain: true,
                prevNextButtons: false,
                pageDots: false,
                freeScroll: false,
                adaptiveHeight: false,
            });
        });

        if (_.isNull(this.prev) === false && _.isNull(this.next) === false) {
            this.initControls();
        }
    }

    initControls() {
        this.next.addEventListener('click', (e) => {
            this.quoteFlkty.next(true, false);
            this.logoFlkty.next(true, false);
            this.imageFlkty.next(true, false);
        });

        this.prev.addEventListener('click', (e) => {
            this.quoteFlkty.previous(true, false);
            this.logoFlkty.previous(true, false);
            this.imageFlkty.previous(true, false);
        });
    }
}

module.exports = TestimonialsSlider;