class NewsCTASlider {
  constructor() {
    this.carousel = document.querySelector(".c-recent-news-cta__grid-inner");

    if (_.isNull(this.carousel) === false) {
      // this.initCarousel();
    }
  }

  initCarousel() {
    this.flkty = new Flickity(this.carousel, {
      cellAlign: "left",
      contain: true,
      prevNextButtons: false,
      pageDots: false,
      watchCSS: true,
      freeScroll: true,
      adaptiveHeight: false
    });

    this.flkty.on("dragStart", (event, pointer) => {
      document.ontouchmove = e => {
        e.preventDefault();
      };
    });

    this.flkty.on("dragEnd", (event, pointer) => {
      document.ontouchmove = e => {
        return true;
      };
    });
  }
}

module.exports = NewsCTASlider;
