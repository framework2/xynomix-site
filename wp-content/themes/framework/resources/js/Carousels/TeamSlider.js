class TeamSlider {

    constructor() {

        this.carousel = document.querySelector('.c-about-team__grid');
        this.prev = document.querySelector('.c-about-team__prev');
        this.next = document.querySelector('.c-about-team__next');

        if (!_.isNull(this.carousel)) {
            this.init();
        }
    }

    init() {
        this.flkty = new Flickity(this.carousel, {
            cellAlign: 'left',
            contain: true,
            prevNextButtons: false,
            pageDots: true,
            watchCSS: true,
            freeScroll: true,
            adaptiveHeight: false,
        });

        this.flkty.on('dragStart', (event, pointer) => {
            document.ontouchmove = (e) => {
                e.preventDefault();
            };
        });

        this.flkty.on('dragEnd', (event, pointer) => {
            document.ontouchmove = (e) => {
                return true;
            };
        });

        if (_.isNull(this.prev) === false && _.isNull(this.next) === false) {
            this.initControls();
        }
    }

    initControls() {
        this.next.addEventListener('click', (e) => {
            this.flkty.next(true, false);
        }, false);

        this.prev.addEventListener('click', (e) => {
            this.flkty.previous(true, false);
        }, false);
    }

}

module.exports = TeamSlider;