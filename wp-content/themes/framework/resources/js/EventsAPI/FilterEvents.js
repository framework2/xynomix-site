class FilterEvents {

    constructor(Animations = null) {

        this.Animations = Animations;
        this.categories = document.querySelectorAll('.c-events-filter__category');
        this.dates = document.querySelectorAll('.c-events-filter__date');
        this.container = document.querySelector('.c-checkerboard');
        this.btn = document.querySelector('.c-events-filter__filterBtn');
        this.categoryFilter = null;
        this.dateFilter = null;
        this.dropdown = document.querySelector('.c-events-filter__dropdown');
        this.dropdownBtn = document.querySelector('.c-events-filter__btn');

        if (!_.isNull(this.categories)) {
            this.filterCategories();
        }

        if (!_.isNull(this.dates)) {
            this.filterDates();
        }

        if (!_.isNull(this.btn)) {
            this.applyFilter();
        }

    }

    applyFilter() {
        this.btn.addEventListener('click', () => {
            this.dropdown.classList.remove('is-open');
            this.dropdownBtn.classList.remove('active');
            this.dropdown.style.removeProperty('max-height', '0px');
            this.fetchEvents(this.categoryFilter, this.dateFilter);
        });
    }

    filterCategories() {
        Array.prototype.forEach.call(this.categories, (el, i) => {
            el.addEventListener('click', () => {
                const category = el.getAttribute('data-category');
                const btn = el.querySelector('.btn');

                if (btn.classList.contains('active')) {
                    btn.classList.remove('active');
                    this.categoryFilter = null;
                } else {
                    this.removeActiveStates(this.categories);
                    btn.classList.add('active');
                    this.categoryFilter = category;
                }
            });
        });
    }

    filterDates() {
        Array.prototype.forEach.call(this.dates, (el) => {
            el.addEventListener('click', () => {
                const year = el.getAttribute('data-year');
                const month = el.getAttribute('data-month');
                const btn = el.querySelector('.btn');

                if (btn.classList.contains('active')) {
                    btn.classList.remove('active');
                    this.dateFilter = null;
                } else {
                    this.removeActiveStates(this.dates);
                    btn.classList.add('active');
                    this.dateFilter = {
                        year,
                        month,
                    };
                }
            });
        });
    }

    removeActiveStates(items) {
        Array.prototype.forEach.call(items, (el, i) => {
            const btn = el.querySelector('.btn');
            btn.classList.remove('active');
        });
    }

    fetchEvents(category = null, date = null) {

        axios.get(`${config.api_uri}filtered-events/event`, {

            params: {
                category,
                date,
            },

        }).then((response) => {

            if (response.data.success === true) {
                document.body.scrollTop = document.documentElement.scrollTop = 0;
                const template = document.querySelector('#FilteredEvents');
                const compiled = _.template(template.innerHTML);
                this.container.innerHTML = compiled({ events: response.data.events });
            } else {
                const template = document.querySelector('#NoEvents');
                const compiled = _.template(template.innerHTML);
                this.container.innerHTML = compiled();
            }

            window.PubSub.publish('fireAnimations', {
                el: this.container,
            });

        }).catch((error) => {
            console.log(error);
        });

    }
}

module.exports = FilterEvents;