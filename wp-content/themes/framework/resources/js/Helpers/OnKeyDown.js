module.exports = function(e) {
    let bodyEl = document.body;
    e = e || window.event;

    if (e.keyCode == '90') {
        // z arrow
        if(bodyEl.classList.contains('show-brkpt')){
            bodyEl.classList.remove('show-brkpt');
        }else{
            bodyEl.classList.add('show-brkpt');
        }
    }
}