/**
 * Dependencies
 */

import PubSub from "./Architectural/PubSub";

const _ = require("lodash");
window.Flickity = require("flickity");
window.scrollmonitor = require("scrollmonitor");
window.axios = require("axios");
window.imagesLoaded = require("imagesloaded");

/**
 * variables
 */
const MobileMenu = require("./Click/MobileMenu");
const ViewBio = require("./Click/ViewBio");
const TeamSlider = require("./Carousels/TeamSlider");
const IndustrySlider = require("./Carousels/IndustrySlider");
const HeroSlider = require("./Carousels/HeroSlider");
const OffsetSlider = require("./Carousels/BucketsSlider");
const CaseStudiesCTA = require("./Carousels/CaseStudiesCTA");
const TestimonialsSlider = require("./Carousels/TestimonialsSlider");
const NewsCTASlider = require("./Carousels/NewsCTASlider");
const HeaderFill = require("./Scroll/HeaderFill");
const Breakpoint = require("./Responsive/Breakpoint");
const JobAccordion = require("./Click/JobAccordion");
const FAQsAccordion = require("./Click/FAQsAccordion");
const FilterAccordion = require("./Click/FilterAccordion");
const MenuHover = require("./Hover/MenuHover");
const GoogleMap = require("./Map/GoogleMap");
const Animations = require("./Animations/Animations");
const ResourcesCarousel = require("./Carousels/ResourcesCarousel");
const LandingSliders = require("./Carousels/LandingSliders");
const FilterEvents = require("./EventsAPI/FilterEvents");

window.Breakpoint = Breakpoint;
Breakpoint.init();
window.addEventListener(
  "resize",
  () => {
    Breakpoint.refreshValue();
  },
  false
);

window.config = {
  api_uri: "/wp-json/xynomix/api/v1/"
};

window.PubSub = new PubSub();

/* turbolinks setup */
(() => {
  window.Turbolinks = require("turbolinks");
  window.Turbolinks.start();
  window.Turbolinks.setProgressBarDelay(0);
})();

/*
 * add in our responsive grid
 * for testing purposes // OnKeyDown
 */
document.onkeydown = require("./Helpers/OnKeyDown");

/*
 * Our Object to run all this
 * which is a class to help
 * us work with resizes
 */
class FwJs {
  constructor() {
    this.initLoad = false;
    this.reloaders = [];
  }

  init() {
    this.initLoad = true;
    this.reloaders.push(new HeaderFill());
    new HeroSlider();
    new OffsetSlider();
    new CaseStudiesCTA();
    new TestimonialsSlider();
    new NewsCTASlider();
    new ViewBio();
    new TeamSlider();
    new IndustrySlider();
    new MobileMenu();
    new GoogleMap();
    new JobAccordion();
    new MenuHover();
    new ResourcesCarousel();
    new LandingSliders();
    new FAQsAccordion();
    //new FilterEvents(new Animations());
    new FilterEvents();
    new FilterAccordion();
    new Animations();
  }

  addPreloaderClass(event) {}

  removePreloaderClass() {
    // const bodyEl = document.body;
    // const preLoader = document.querySelector('.l-preloader');
    // if (bodyEl.classList.contains('is-preloading')) {
    //     setTimeout(() => {
    //         bodyEl.classList.remove('is-preloading');
    //         preLoader.classList.remove('active');
    //         new Animations();
    //     }, 750);
    // }
  }

  /*
   * the resizer runs all classes that assign
   * themselves the resize method
   */
  resize() {
    this.reloaders.forEach((element, index) => {
      if (typeof element.resize === "function") {
        element.resize();
      }
    });
  }
}

// CREATE the main object
const FwJsObj = new FwJs();
FwJsObj.init();

/*
 * turbolinks
 */
document.addEventListener("turbolinks:click", event => {
  FwJsObj.addPreloaderClass(event);
  // if (app.environment === 'production') {
  //     if (window.ga) {
  //         window.ga(
  //             'send',
  //             'pageview',
  //             window.location.origin + event.target.pathname,
  //         );
  //     }
  // }
});

document.addEventListener("turbolinks:before-cache", () => {});
document.addEventListener("turbolinks:before-visit", () => {});
document.addEventListener("turbolinks:before-render", () => {});
document.addEventListener("turbolinks:request-start", () => {});
document.addEventListener("turbolinks:load", () => {
  FwJsObj.removePreloaderClass();
});

/*
 * one resize event listener needed which runs through all Our
 * classes that want to run resize events
 */
window.addEventListener(
  "resize",
  _.debounce(() => FwJsObj.resize(), 300),
  false
);

/*
 * lets get it ready (as in only use when dom content is ready)
 *
 * @param fn the function being passed in to be used
 */
function ready(fn) {
  if (
    document.attachEvent
      ? document.readyState === "complete"
      : document.readyState !== "loading"
  ) {
    fn();
  } else {
    document.addEventListener("DOMContentLoaded", fn);
  }
}

//ready(FwJsObj.init());
