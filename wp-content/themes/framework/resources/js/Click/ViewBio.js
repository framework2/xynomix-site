class ViewBio {

    constructor() {

        this.teamMembers = document.querySelectorAll('.c-about-team__item');

        if (!_.isNull(this.teamMembers)) {
            this.init();
        }
    }

    init() {

        Array.prototype.forEach.call(this.teamMembers, (el, i) => {

            const bio = el.querySelector('.c-about-team__bio ');
            const btn = el.querySelector('.btn--bio');

            btn.addEventListener('click', (e) => {

                e.preventDefault();

                if (el.classList.contains('is-open')) {
                    el.classList.remove('is-open');
                    btn.classList.remove('active');
                } else {
                    el.classList.add('is-open');
                    btn.classList.add('active');
                }
            });
        });
    }
}

module.exports = ViewBio;