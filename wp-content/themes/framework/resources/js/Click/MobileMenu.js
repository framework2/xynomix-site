class MobileMenu {
   
    constructor() {
        
        this.btn = document.querySelector('.l-header__menu-btn');
        this.menu = document.querySelector('.c-main-menu-wrapper');
        this.body = document.body;

        if (!_.isNull(this.btn) && !_.isNull(this.menu)) {
            this.init();
        }
    }

    init() {
        this.btn.addEventListener('click', () => {
            if (this.btn.classList.contains('is-open')) {
                this.close();
            } else {
                this.open();
            }
        });
    }

    open() {
        this.btn.classList.add('is-open');
        this.menu.classList.add('is-open');
        this.body.classList.add('overflow-hidden');
    }

    close() {
        this.btn.classList.remove('is-open');
        this.menu.classList.remove('is-open');
        this.body.classList.remove('overflow-hidden');
    }
}

module.exports = MobileMenu;