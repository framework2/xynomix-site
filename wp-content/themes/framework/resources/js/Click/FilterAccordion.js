class FilterAccordion {
    
    constructor() {
        
        this.btn = document.querySelector('.c-events-filter__btn');
        this.dropdown = document.querySelector('.c-events-filter__dropdown');
     
        if (!_.isNull(this.btn) && !_.isNull(this.dropdown)) {
            this.init();
        }
    }

    init() {
        this.btn.addEventListener('click', () => {
            const height = this.dropdown.scrollHeight;


            if (this.dropdown.classList.contains('is-open')) {
                this.dropdown.classList.remove('is-open');
                this.btn.classList.remove('active');
                this.dropdown.style.removeProperty('max-height', '0px');
            } else {
                this.dropdown.classList.add('is-open');
                this.btn.classList.add('active');
                this.dropdown.style.maxHeight = `${height}px`;
            }

        });
    }
}

module.exports = FilterAccordion;