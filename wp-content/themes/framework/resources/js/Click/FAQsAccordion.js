class FAQsAccordion {

    constructor() {

        this.items = document.querySelectorAll('.c-resources-faqs__item');

        if (!_.isNull(this.items)) {
            this.init();
        }
    }

    init() {
        Array.prototype.forEach.call(this.items, (el, i) => {

            const clickArea = el.querySelector('.c-resources-faqs__title-bar');
            const btn = el.querySelector('.btn--accordion');
            const reveal = el.querySelector('.c-resources-faqs__overview');
            const height = reveal.scrollHeight;

            clickArea.addEventListener('click', (e) => {
                if (el.classList.contains('is-open')) {
                    el.classList.remove('is-open');
                    btn.classList.remove('active');
                    reveal.style.removeProperty('max-height', '0px');
                } else {
                    el.classList.add('is-open');
                    btn.classList.add('active');
                    reveal.style.maxHeight = `${height}px`;
                }
            });
        });
    }
}

module.exports = FAQsAccordion;