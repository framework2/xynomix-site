class MenuHover {

    constructor() {
        this.flkty = [];
        this.dropdowns = document.querySelectorAll('.c-main-menu__dropdown');

        if (!_.isNull(this.sliders)) {
            this.init();
        }
    }

    init() {
            
        Array.prototype.forEach.call(this.dropdowns, (el, i) => {

            const slider = el.querySelector('.c-main-menu__dropdown-slider');
            const menuItems = el.querySelectorAll('.c-main-menu__submenu-item');
            
            this.flkty[i] = new Flickity(slider, {
                draggable: false,
                cellAlign: 'left',
                contain: true,
                prevNextButtons: false,
                pageDots: false,
                //watchCSS: true,
                freeScroll: false,
                adaptiveHeight: false,
            });

            Array.prototype.forEach.call(menuItems, (el2, x) => {
                el2.addEventListener('mouseover', () => {
                    this.flkty[i].select(x + 1, true, false);
                });
            });
        });
    }
}

module.exports = MenuHover;