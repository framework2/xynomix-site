class GoogleMap {

    constructor() {
        this.cacheDom();
        this.resize();
    }

    cacheDom() {
        this.map = null;
        this.mapCanvas = document.querySelector('#googleCanvas');
    }

    resize() {
        if (_.isUndefined(window.Breakpoint.grpInt) === false) {
            if (window.Breakpoint.grpInt <= 3) {
                this.initMap('mobile');
            } else {
                this.initMap('desktop');
            }
        }
    }

    initMap(size) {
        if (_.isNull(this.mapCanvas) === false) {

            const lat = parseFloat(this.mapCanvas.getAttribute('data-lat'));
            const lng = parseFloat(this.mapCanvas.getAttribute('data-lng'));
            const address = this.mapCanvas.getAttribute('data-address');

            this.map = new google.maps.Map(this.mapCanvas, {
                center: { lat: lat, lng: lng },
                zoom: 17,
                disableDefaultUI: true,
            });

            const marker = new google.maps.Marker({
                position: this.map.center,
                map: this.map,
                title: address,
                draggable: false,
                icon: app.map_icon.desktop,
            });
        }
    }
}

module.exports = GoogleMap;