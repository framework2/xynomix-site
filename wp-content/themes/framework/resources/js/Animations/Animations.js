class Animations {

    constructor() {

        window.PubSub.subscribe('fireAnimations', this.fire, this);

        this.cacheDom();

        if (!_.isNull(this.elements)) {
            this.init();
        }
    }

    cacheDom() {
        this.elements = document.querySelectorAll('[data-animate]');
    }

    init() {

        Array.prototype.forEach.call(this.elements, (el, i) => {

            const watchedItem = scrollMonitor.create(el, -150);

            if (el.hasAttribute('data-delay')) {
                el.style.transitionDelay = `${el.getAttribute('data-delay')}ms`;
            }

            watchedItem.enterViewport(() => {
                el.classList.add('animate');
            });
        });
    }

    fire(el) {
        this.cacheDom();
        this.init();
    }
}

module.exports = Animations;