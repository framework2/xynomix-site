<?php 
    $social = get_field('social', 'option');
    $details =  get_field('company_details', 'option');
    $awards =  get_field('footer_awards', 'option');
?>

<footer class="l-footer bg-black small:pt-8 xxlarge:pt-12 pb-20">
    <div class="l-container">
        <div class="row-inner flex small:flex-col-reverse xxlarge:flex-row justify-between small:mb-6 xxlarge:mb-13">
            <div class="small-12 xxlarge-4 column">
                <a href="<?php echo home_url(); ?>" class=" block l-footer__logo relative">    
                    <?php //Framework::svgIcon('xynomix-logo-full-landscape', '0 0 241 38'); ?>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/svgs/xynomix-footer.svg"/>
                </a>
            </div>
            <div class="small:mb-9 xxlarge:mb-0 small-12 xxlarge-6 column">
                <div class="l-footer__form">
                    <?php echo do_shortcode('[contact-form-7 id="198" title="Footer Newsletter"]'); ?>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="row-inner xxlarge:flex">
            <div class="small-12 xxlarge-4 column small:mb-9 xxlarge:mb-0">
                <ul class="list-reset small:mb-4 xxlarge:mb-5">
                    <li class="xynomix-copy-xs text-white">Xynomix Limited,</li>
                    <li class="xynomix-copy-xs text-white">Electron House, Bridge Street,</li>
                    <li class="xynomix-copy-xs text-white">Sandiacre, Nottingham UK, NG10 5BA</li>
                </ul>

                <p class="xynomix-copy-xs text-white">© Xynomix Limited <?php echo date("Y"); ?></p> 
            </div>
            <div class="small-12 xxlarge-2 column small:mb-9 xxlarge:mb-0">
                <span class="xynomix-copy-xs text-white mb-2 block font-semibold">Follow us</span> 
                <div class="flex items-center">
                    <?php if ($social['linkedin']) : ?>
                        <a href="<?php echo $social['linkedin']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-linkedin text-white relative">
                            <?php Framework::svgIcon('linkedin', '0 0 17 17'); ?>
                        </a>
                    <?php endif; ?>
                    <?php if ($social['google+']) : ?>
                        <a href="<?php echo $social['google+']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-googleplus text-white relative">
                            <?php Framework::svgIcon('googleplus', '0 0 25 16'); ?>
                        </a>
                    <?php endif; ?>
                    <?php if ($social['twitter']) : ?>
                        <a href="<?php echo $social['twitter']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-twitter text-white relative">
                            <?php Framework::svgIcon('twitter', '0 0 19 15'); ?>
                        </a>
                    <?php endif; ?>
                    <?php if ($social['facebook']) : ?>
                        <a href="<?php echo $social['facebook']; ?>" target="_blank" class="l-footer__social-icon l-footer__social-facebook text-white relative">
                            <?php Framework::svgIcon('facebook', '0 0 18 16'); ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="small-12 xxlarge-6 column small:mb-10 xxlarge:mb-0">
                <div class="row-inner"> 
                    <div class="column small-12 xxlarge-4 small:mb-10 xxlarge:mb-0">
                        <div class="small:mb-7 xxlarge:mb-11">
                            <h3 class="xynomix-copy-xs text-white font-semibold mb-2">Customer Services</h3>
                            <ul class="mb-0 list-reset">
                                <li>
                                    <a href="tel: <?php echo $details['phone_number']; ?>" class="xynomix-copy-xs text-white l-footer__link">
                                        <img class="l-footer__contact__tel mb-2" src="<?= get_theme_file_uri('/assets/images/contact/tel-white.png'); ?>" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="mailto: <?php echo $details['email_address']; ?>" class="xynomix-copy-xs text-white l-footer__link">
                                        <img class="l-footer__contact__email" src="<?= get_theme_file_uri('/assets/images/contact/email-white.png'); ?>" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="small-12 xxlarge-6 small:flex">
                        <div class="column small-6">
                            <h3 class="xynomix-copy-xs text-white font-semibold mb-2">Site Map</h3>
                            <?php get_template_part('template-parts/sitewide/footer-sitemap'); ?>
                        </div>

                        <div class="column small-6 xxlarge-6">
                            <h3 class="xynomix-copy-xs text-white font-semibold mb-2">Links</h3>
                            <?php get_template_part('template-parts/sitewide/footer-links'); ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <?php if (!empty($awards)): ?>
            <div class="large:flex large:items-center large:mt-6">
            <?php foreach ($awards as $award): ?>
                <?php if (!empty($award['link'])): ?>
                    <div class="l-footer__award">
                        <a href="<?= $award['link'] ?>" target="_blank" rel="noopener">
                            <img class="l-footer__award__img" src="<?= $award['image'] ?>" alt="" style="width: auto">
                        </a>
                    </div>
                <?php elseif (!empty($award['image'])): ?>
                    <div class="l-footer__award">
                        <img class="l-footer__award__img" src="<?= $award['image'] ?>" alt="" style="width: auto">
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>
