
<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', 'alt'); ?>

<?php get_template_part('template-parts/events-internal/events-internal', 'content'); ?>

<div class="small:mb-10 xlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/events-cta', null); ?>
</div>

<?php get_footer(); ?>