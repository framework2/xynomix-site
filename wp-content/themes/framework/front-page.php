<?php get_header(); ?>

<?php get_template_part('template-parts/home/home','hero'); ?>
<?php get_template_part('template-parts/home/home','buckets'); ?>

<div class="mb-12">
    <?php get_template_part('template-parts/sitewide/services-cta', null); ?>
</div>

<?php get_template_part('template-parts/home/home','clients'); ?>

<div class="small:mb-12 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/case-studies-cta', null); ?>
</div>

<div class="small:mb-8 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/testimonials', null); ?>
</div>


<div class="small:mb-8 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/recent-news-cta', null); ?>
</div>

<?php get_template_part('template-parts/sitewide/contact-cta', null); ?>

<?php get_footer(); ?>