<?php 
    /* Template Name: Events Template */
?>

<?php get_header(); ?>

<div class="small:mt-10 xxlarge:mt-14">
    <?php 
        $post_type = 'events';
        include_once( locate_template('template-parts/sitewide/checkerboard.php')); 
    ?>
</div>

<div class="small:mt-6 large:-mt-4">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>
<?php get_footer(); ?>