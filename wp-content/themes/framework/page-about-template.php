<?php 
    /* Template Name: About Template */
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<?php get_template_part('template-parts/about/about', 'intro'); ?>
<?php get_template_part('template-parts/about/about', 'split'); ?>
<?php get_template_part('template-parts/about/about', 'team'); ?>

<div class="small:mb-10 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/industry-sectors', null); ?>
</div>

<?php get_template_part('template-parts/about/about', 'careers'); ?>
<?php get_template_part('template-parts/about/about', 'social-responsibility'); ?>

<div class="small:mt-4 xxlarge:-mt-4">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>
<?php get_footer(); ?>