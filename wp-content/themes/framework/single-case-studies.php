
<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', 'alt'); ?>

<?php get_template_part('template-parts/sitewide/internal-layout', null); ?>

<div class="small:mb-12 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/case-studies-cta', null); ?>
</div>

<?php get_footer(); ?>