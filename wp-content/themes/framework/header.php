<!doctype html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- dns prefetch -->
	<link href="//www.google-analytics.com" rel="dns-prefetch">

	<!-- meta -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta name="turbolinks-cache-control" content="no-cache">

	<?php if (is_page_template('page-contact-template.php')) : ?>
		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCa4t_URP4Ed0Dn3YE_C6mJaXQydoo2BcY"
	  type="text/javascript"></script>
	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<script type="text/javascript" src="https://secure.leadforensics.com/js/12957.js"></script>
<noscript><img src="https://secure.leadforensics.com/12957.png" alt="" style="display:none;" /></noscript>

<?php get_template_part('template-parts/sitewide/svg-sprite'); ?>
<?php get_template_part('template-parts/sitewide/preloader'); ?>

<header class="l-header fixed pin-t pin-l pin-r w-full z-30 bg-white">
	<div class="l-header__main flex items-center justify-between small:py-3 small:px-4 xxlarge:px-6 xxlarge:py-0">
		<a href="<?php echo home_url(); ?>" class="l-header__logo inline-block relative z-10">
			<!-- <img class="l-header__logo--colour absolute pin-t pin-l w-full active" src="<?php //echo get_template_directory_uri(); ?>/assets/images/svgs/xynomix-mark.svg"/> -->
			<img class="l-header__logo w-full" src="<?php echo get_template_directory_uri(); ?>/assets/images/svgs/xynomix-logo-header.png"/>	
		</a>

		<div class="c-main-menu-wrapper xxlarge:flex small:flex-col xxlarge:flex-row small:items-end xxlarge:items-center small:px-4 xxlarge:px-0">
			<?php get_template_part('template-parts/sitewide/main-menu'); ?>
			<div class="small:flex justify-end hidden">
				<?php Framework::btnSearch(null, null, null, 'span'); ?>
			</div>
		</div>

		<span class="l-header__menu-btn small:inline-block xxlarge:hidden"></span>
	</div>

	<?php 
if (is_page_template('page-events-template.php')) {
	get_template_part('template-parts/events/events-filter', null);
}
?>

</header>