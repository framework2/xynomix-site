<?php get_header(); ?>


<?php if ( have_posts() ) : ?>

	<h1><?php printf( __( 'Search Results for: %s', 'theframework' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

	<?php while ( have_posts() ) : the_post(); ?>

		<h1><?php the_title(); ?></h1>

		<?php the_content(); ?>

	<?php endwhile; ?>

<?php else: ?>

	<h1><?php _e( 'Nothing Found', 'theframework' ); ?></h1>

<?php endif; ?>


<?php get_footer(); ?>