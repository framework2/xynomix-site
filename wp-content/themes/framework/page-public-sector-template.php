<?php 
    /* Template Name: Public Sector Template */
?>

<?php get_header(); ?>
<?php get_template_part('template-parts/sitewide/hero-internal', null); ?>

<?php get_template_part('template-parts/public-sector/public-sector', 'opening'); ?>
<?php get_template_part('template-parts/public-sector/public-sector', 'intro'); ?>
<?php get_template_part('template-parts/public-sector/public-sector', 'feature-text'); ?>

<div class="small:py-10 xxlarge:py-20 c-public-sector-services">
    <?php get_template_part('template-parts/sitewide/services-cta', null); ?>
</div>

<div>
    <?php get_template_part('template-parts/sitewide/industry-sectors', null); ?>
</div>

<?php get_template_part('template-parts/public-sector/public-sector', 'accreditations'); ?>

<div class="small:py-10 xxlarge:py-20 c-public-sector-services">
    <?php get_template_part('template-parts/sitewide/case-studies-cta', null); ?>
</div>

<div class="small:py-10 xxlarge:py-20 bg-white c-public-sector-services">
    <?php get_template_part('template-parts/sitewide/testimonials', null); ?>
</div>


<div class="small:mb-8 xxlarge:mb-20">
    <?php get_template_part('template-parts/sitewide/recent-news-cta', null); ?>
</div>

<div class="small:mt-6 large:-mt-4">
    <?php get_template_part('template-parts/sitewide/contact-cta', null); ?>
</div>
<?php get_footer(); ?>