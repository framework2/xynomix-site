=== The Framework Theme ===
Contributors: The Framework
Requires at least: WordPress 4.7
Tested up to: WordPress 4.7
Version: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

The Framework White Label Theme for developing new Wordpress Sites.

== Changelog ==

= 1.0 =
* Released: February 12, 2017

Initial release

= 2.0 =
* Released: September 27, 2017

## Installation Commands

```im
cd wp-content/themes/framework
```

```im
npm install --save-dev
```

```im
bower install foundation-sites
```

```im
gulp watch
```

## PHP methods/functions
```php
// using svg Icon
Framework::svgIcon('icon-cart','0 0 29.27 24.4');
```

```php
// creating new custom post type (example below)
$custom_post_types->create('testimonial', 'Testimonial', 'Testimonials', [
	'has_archive'   => true,
	'rewrite'       => ['slug' => 'testimonials'],
	'menu_icon'     => 'dashicons-groups',
	'menu_position' => 27,
	'show_in_rest'  => false,
	'supports'      => ['title', 'thumbnail', 'editor']
]);
````

```php
// attaching a taxonomy to custom post type
$custom_taxonomy->add('testimonial-category', 'Testimonial Category', 'Testimonial Categories', 'testimonial', [
    'hierarchical' => true,
    'public'       => true,
    'publicly_queryable' => false
]);
```

```php
// creating new api endpoints (example below)
$routes->endpoint('projects/', '/all',
	array(
		'methods' => 'GET',
		'callback' => [\TheFramework\Test::class, 'hello']
	)
);
```

```php
// loading svg files
Framework::svgIcon('icon-logo','0 0 141.57 141.57');
```

## Vuejs app
```php
// pass array of urls
if ( class_exists( 'CDN_Enabler_Rewriter' ) ) {
    return UpdateUrls::perform($fields);
}
```

## Local Server

Valet
```im
brew services start mysql
```

## Reference Materials
[Function Reference/register post type](https://codex.wordpress.org/Function_Reference/register_post_type)

[register_rest_route](https://developer.wordpress.org/reference/functions/register_rest_route)